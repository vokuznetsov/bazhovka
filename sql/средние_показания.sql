insert into address_indications (t01, t02, monthnum, address_id, inserted_at, updated_at, avg_calculated)
select
    round(avg(t01_consumption)) + pi.t01 as t01,
    round(avg(t02_consumption)) + pi.t02 as t02,
    202002 as monthnum,
    c.address_id,
    now(),
    now(),
    true as avg_calculated
from address_calculations c
join addresses a on a.id = c.address_id
join address_indications pi on pi.address_id = c.address_id
where owner is not NULL
and c.address_id not in (4, 5)
and not a.system
and not a.archive
and not exists (select 1 from address_indications ai where ai.address_id = a.id and ai.monthnum = 202002)
and pi.monthnum = 202001
group by c.address_id, pi.id;

