\copy users(id, name, role, email, encrypted_password, created_at, updated_at) to users
\copy addresses(id, name, owner, system, account_num, fider_num, created_at, updated_at) to addresses
\copy addresses_users(address_id, user_id) to address_users
\copy calculations(id, monthnum, f1t01curr, f1t02curr, f1t01prev, f1t02prev, f2t01curr, f2t02curr, f2t01prev, f2t02prev, lighting_t01_curr, lighting_t02_curr, lighting_t01_prev, lighting_t02_prev, entrance_t01_curr, entrance_t02_curr, entrance_t01_prev, entrance_t02_prev, multiple_factor, loss_coeff, loss_const, t01_rate, t02_rate, lighting_proportion, entrance_proportion, t01_consumption, t02_consumption, t01_loss_proportional, t02_loss_proportional, gen_t01_sum, gen_t02_sum, lighting_t01_consumption, lighting_t02_consumption, entrance_t01_consumption, entrance_t02_consumption, lighting_t01_sum, lighting_t02_sum, entrance_t01_sum, entrance_t02_sum, t01_loss, t02_loss, t01_counters_sum, t02_counters_sum, calculation_class, created_at, updated_at) to calculations
\copy address_calculations(id, address_id, calculation_id, t01_curr, t01_prev, t02_curr, t02_prev, t01_loss, t02_loss, t01_sum, t02_sum, t01_consumption, t02_consumption, t01_correction, t02_correction) to address_calculations
\copy address_corrections(id, address_id, t01, t02, monthnum, created_at, updated_at) to address_corrections
\copy indications(id, t01, t02, monthnum, address_id, created_at, updated_at) to indications

\copy users(id, name, role, email, encrypted_password, inserted_at, updated_at) from users
\copy addresses(id, name, owner, system, account_num, fider_num, inserted_at, updated_at) from addresses
\copy address_users(address_id, user_id) from address_users
\copy calculations(id, monthnum, f1t01curr, f1t02curr, f1t01prev, f1t02prev, f2t01curr, f2t02curr, f2t01prev, f2t02prev, lighting_t01_curr, lighting_t02_curr, lighting_t01_prev, lighting_t02_prev, entrance_t01_curr, entrance_t02_curr, entrance_t01_prev, entrance_t02_prev, multiple_factor, loss_coeff, loss_const, t01_rate, t02_rate, lighting_proportion, entrance_proportion, t01_consumption, t02_consumption, t01_loss_proportional, t02_loss_proportional, gen_t01_sum, gen_t02_sum, lighting_t01_consumption, lighting_t02_consumption, entrance_t01_consumption, entrance_t02_consumption, lighting_t01_sum, lighting_t02_sum, entrance_t01_sum, entrance_t02_sum, t01_loss, t02_loss, t01_counters_sum, t02_counters_sum, calculation_class, inserted_at, updated_at) from calculations
\copy address_calculations(id, address_id, calculation_id, t01_curr, t01_prev, t02_curr, t02_prev, t01_loss, t02_loss, t01_sum, t02_sum, t01_consumption, t02_consumption, t01_correction, t02_correction) from address_calculations
\copy address_corrections(id, address_id, t01, t02, monthnum, inserted_at, updated_at) from address_corrections
\copy address_indications(id, t01, t02, monthnum, address_id, inserted_at, updated_at) from indications

select setval('users_id_seq', (select max(id) + 1 from users));
select setval('addresses_id_seq', (select max(id) + 1 from addresses));
select setval('address_users_id_seq', (select max(id) + 1 from address_users));
select setval('calculations_id_seq', (select max(id) + 1 from calculations));
select setval('address_calculations_id_seq', (select max(id) + 1 from address_calculations));
select setval('address_corrections_id_seq', (select max(id) + 1 from address_corrections));
select setval('address_indications_id_seq', (select max(id) + 1 from address_indications));
