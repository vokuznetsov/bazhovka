select
    c.monthnum,
    round((f1t01curr - f1t01prev) * multiple_factor + loss_const + loss_coeff / 100 * (f1t01curr - f1t01prev) * multiple_factor) as f1T1,
    sum(ac.t01_consumption) FILTER (WHERE a.fider_num = 1) as ind1T1,
    round((f1t02curr - f1t02prev) * multiple_factor + loss_const + loss_coeff / 100 * (f1t02curr - f1t02prev) * multiple_factor) as f1T2,
    sum(ac.t02_consumption) FILTER (WHERE a.fider_num = 1) as ind1T2,

    round((f2t01curr - f2t01prev) * multiple_factor + loss_const + loss_coeff / 100 * (f2t01curr - f2t01prev) * multiple_factor) as f2T1,
    sum(ac.t01_consumption) FILTER (WHERE a.fider_num = 2) as ind2T1,
    round((f2t02curr - f2t02prev) * multiple_factor + loss_const + loss_coeff / 100 * (f2t02curr - f2t02prev) * multiple_factor) as f2T2,
    sum(ac.t02_consumption) FILTER (WHERE a.fider_num = 2) as ind2T2
from calculations c
inner join address_calculations ac on ac.calculation_id = c.id
inner join addresses a on ac.address_id = a.id
where not a.system
group by f1t01curr, f1t01prev, f1t02curr, f1t02prev, c.id, c.monthnum
order by c.monthnum desc;

select
    c.monthnum,
    round((f1t01curr - f1t01prev) * multiple_factor) as f1T1,
    sum(ac.t01_consumption) FILTER (WHERE a.fider_num = 1) as ind1T1,
    round((f1t02curr - f1t02prev) * multiple_factor) as f1T2,
    sum(ac.t02_consumption) FILTER (WHERE a.fider_num = 1) as ind1T2,

    round((f2t01curr - f2t01prev) * multiple_factor) as f2T1,
    sum(ac.t01_consumption) FILTER (WHERE a.fider_num = 2) as ind2T1,
    round((f2t02curr - f2t02prev) * multiple_factor) as f2T2,
    sum(ac.t02_consumption) FILTER (WHERE a.fider_num = 2) as ind2T2
from calculations c
inner join address_calculations ac on ac.calculation_id = c.id
inner join addresses a on ac.address_id = a.id
where not a.system
group by f1t01curr, f1t01prev, f1t02curr, f1t02prev, c.id, c.monthnum
order by c.monthnum desc;

update address_indications

select address_id, avg(t01) as t01, avg(t02)
from address_indications i
join addresses a on a.id = i.address_id
where i.monthnum < 201812
  and not a.system
  and not a.archive
  and not exists (select 1 from address_indications ai where ai.address_id = a.id and ai.monthnum = 201812)
group by address_id