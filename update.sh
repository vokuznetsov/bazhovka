#!/bin/sh

# Exit if any statement returns a non-true return value
set -e

# Pull new version
git pull

# Initial setup
MIX_ENV=prod mix deps.get --only prod
MIX_ENV=prod mix compile

# Compile assets
nodejs node_modules/brunch/bin/brunch build --production
MIX_ENV=prod mix phoenix.digest

# Custom tasks (like DB migrations)
MIX_ENV=prod mix ecto.migrate

# Create folders
mkdir -p tmp/pids
mkdir -p log

# Finally restart the server
if [ -f tmp/pids/server.pid ]; then
    kill $(cat tmp/pids/server.pid)
fi
MIX_ENV=prod PORT=4000 elixir --erl "+K true" --detached -e 'File.write("tmp/pids/server.pid",System.get_pid)' -S mix phoenix.server
