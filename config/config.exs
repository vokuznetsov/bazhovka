# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :bazhovka,
  ecto_repos: [Bazhovka.Repo]

# Configures the endpoint
config :bazhovka, Bazhovka.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "PK6nhjDMw6NQxwk7/kDMy3Lnrv+qY+dlabrtFapEYLbnqbrrUaEItTHP+Y7VevXU",
  render_errors: [view: Bazhovka.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Bazhovka.PubSub,
           adapter: Phoenix.PubSub.PG2]

config :bazhovka,
  access_rights: [
    {Bazhovka.PersonalController, {[:index], :user}},
    {Bazhovka.PostController, {[:show], :user}},
    {Bazhovka.AddressCalculationController, {[:index], :user}},
    {Bazhovka.IndicationController, {[:new, :create, :update], :user}},
    {Bazhovka.IndicationsHistoryController, {[:index], :user}},
    {Bazhovka.AddressPaymentController, {[:index], :user}},
    {Bazhovka.PaymentReceiptController, {[:index, :show], :user}},
    {Bazhovka.SessionController, {[:new, :create, :delete], :all}},
    {Bazhovka.PageController, {[:index], :all}},
    {Bazhovka.PasswordController, {[:reset, :token], :all}},
    {Bazhovka.PasswordController, {[:edit, :update], :user}},
    {Bazhovka.DocumentController, {[:index, :show], :user}},
    {Bazhovka.FolderController, {[:index], :user}},
    {Bazhovka.VoteController, {[:create], :user}}
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :bazhovka, :documents_path,
  Path.join(System.user_home, "bazhovka_docs")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
