defmodule Bazhovka.Access do
  import Plug.Conn
  import Phoenix.Controller

  def init(_opts) do
    Application.get_env(:bazhovka, :access_rights)
    |> Enum.flat_map(fn{controller, {actions, access_level}} ->
      Enum.map(actions, &({{controller, &1}, access_level}))
    end)
    |> Map.new
  end

  def call(conn, access_rights) do
    user = conn.assigns[:current_user]
    controller = conn.private.phoenix_controller
    action = conn.private.phoenix_action

    user_access_level = if user do
      if user.role == "admin", do: :admin, else: :user
    else
      :all
    end

    action_access_level = Map.get(access_rights, {controller, action}, :admin)

    case {user_access_level, action_access_level} do
      {:admin, _} -> conn
      {_, :admin} -> forbidden(conn)
      {_, :all} -> conn
      {:user, :user} -> conn
      {:all, :user} -> forbidden(conn)
    end
  end

  defp forbidden(conn) do
    conn
    |> put_flash(:error, "Доступ к этой странице ограничен")
    |> redirect(to: Bazhovka.Router.Helpers.session_path(conn, :new, redirect_to: conn.request_path))
    |> halt
  end
end
