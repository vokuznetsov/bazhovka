defmodule Bazhovka.Auth do
  import Plug.Conn
  import Comeonin.Bcrypt, only: [checkpw: 2, dummy_checkpw: 0]

  alias Bazhovka.Repo
  alias Bazhovka.User

  def init(opts) do
    opts
  end

  def call(conn, _opts) do
    user_id = get_session(conn, :user_id)
    user = if user_id, do: Repo.get(User, user_id)

    if user && user.role > "" do
      Logger.metadata(user_id: user.id)
      assign(conn, :current_user, user)
    else
      conn
    end
  end

  def logout(conn) do
    configure_session(conn, drop: true)
  end

  def login(conn, email, password) do
    user = Repo.get_by(User, email: String.downcase(email))

    cond do
      user && checkpw(password, user.encrypted_password) && user.role > "" ->
        {:ok, login(conn, user)}
      user ->
        {:error, :unauthorized, conn}
      true ->
        dummy_checkpw()
        {:error, :not_found, conn}
    end
  end

  def login(conn, user) do
    conn
    |> assign(:current_user, user)
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
  end

  defmodule Helpers do
    def current_user(conn) do
      conn.assigns[:current_user]
    end
  end
end
