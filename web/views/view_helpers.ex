defmodule Bazhovka.ViewHelpers do
  alias Bazhovka.Repo
  alias Bazhovka.Month
  alias Bazhovka.Payment
  alias Bazhovka.Address

  use Phoenix.HTML

  import Ecto.Query, only: [from: 2]

  def number_with_precision(%Decimal{} = decimal, precision: p) do
    Decimal.to_float(decimal)
    |> :erlang.float_to_binary(decimals: p)
  end

  def monthname(monthnum) do
    Month.from_num(monthnum).name
  end

  def address_selections(:personal) do
    from(a in Address, where: not a.system, select: [a.name, a.owner, a.id], order_by: a.name)
    |> Repo.all
    |> Enum.map(fn([name, owner, id]) ->
      {"#{name} - #{owner}", id}
    end)
  end

  def month_selections do
    Stream.iterate(Month.current, &Month.prev/1)
    |> Enum.take_while(&(&1.num >= 201501))
    |> Enum.map(fn(month) ->
      {month.name, month.num}
    end)
  end

  def payment_type_selections do
    Enum.map(Payment.types, fn({type, name}) ->
      {name, type}
    end)
  end

  def payment_type_name(payment_type) do
    Payment.types[String.to_existing_atom(payment_type)]
  end

  def format_time(time) do
    Timex.Timezone.convert(time, "Asia/Yekaterinburg")
    |> Timex.format!("%d.%m.%Y %H:%M", :strftime)
  end

  def format_date(nil) do nil end
  def format_date(time) do
    Timex.Timezone.convert(time, "Asia/Yekaterinburg")
    |> Timex.format!("%d.%m.%Y", :strftime)
  end

  def markdown(text) do
    {html, _} = Earmark.as_html(text)
    HtmlSanitizeEx.basic_html(html)
  end

  def paragraphs(text) do
    String.split(text, "\n")
  end
end
