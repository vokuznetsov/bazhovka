defmodule Bazhovka.DocumentView do
  use Bazhovka.Web, :view

  alias Bazhovka.Repo
  alias Bazhovka.Folder

  import Ecto.Query, only: [from: 2]

  def folders_selections() do
    from(f in Folder, select: [f.name, f.id], order_by: f.name)
    |> Repo.all
    |> Enum.map(fn([name, id]) ->
      {name, id}
    end)
  end
end
