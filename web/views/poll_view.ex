defmodule Bazhovka.PollView do
  use Bazhovka.Web, :view

  def paste_code(poll) do
    "$(poll:#{poll.id})"
  end
end
