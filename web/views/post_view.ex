defmodule Bazhovka.PostView do
  use Bazhovka.Web, :view

  def process_vars(conn, text) do
    Regex.replace(~r/\$\(poll:(\d+)\)/, text, fn(_, poll_id) ->
      poll = Bazhovka.PollService.load_polls(String.to_integer(poll_id))
      if poll do
        Phoenix.View.render_to_string(
          Bazhovka.PollView, "poll.html",
          poll: poll,
          admin_mode: false,
          conn: conn
        )
      else
        "<p><em>$(Здесь указана ссылка на опрос №#{poll_id}, но его не существует, возможно он был удалён)</em></p>"
      end
    end)
  end
end
