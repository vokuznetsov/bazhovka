defmodule Bazhovka.CalculationView do
  use Bazhovka.Web, :view

  def gen_sum(calculation) do
    Decimal.add(calculation.gen_t01_sum, calculation.gen_t02_sum)
    |> Decimal.add(calculation.lighting_t01_sum)
    |> Decimal.add(calculation.lighting_t02_sum)
    |> Decimal.add(calculation.entrance_t01_sum)
    |> Decimal.add(calculation.entrance_t02_sum)
  end

  def ind_sum(calculation) do
    Enum.reduce(calculation.address_calculations, Decimal.new(0), fn(ac, sum) ->
      Decimal.add(ac.t01_sum, ac.t02_sum) |> Decimal.add(sum)
    end)
  end
end
