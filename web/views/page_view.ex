defmodule Bazhovka.PageView do
  use Bazhovka.Web, :view

  @max_text_length 600

  def truncate_post(text) do
    Regex.replace(~r/\$\(.+?\)/, text, "")
    |> Curtail.truncate(length: @max_text_length)
  end
end
