defmodule Bazhovka.AddressCalculationView do
  use Bazhovka.Web, :view
  alias Bazhovka.Month

  defmodule Row do
    defstruct [:address_calculation, :month, :t01_curr, :t01_corr, :t01_cons, :t01_loss, :t01_sum, :t02_curr, :t02_corr, :t02_cons, :t02_loss, :t02_sum, :total_sum]
  end

  defp rows(address_calculations) do
    Enum.map(address_calculations, fn(ac) ->
      %Row{
        address_calculation: ac,
        month: Month.from_num(ac.calculation.monthnum).name,
        t01_curr: ac.t01_curr,
        t01_corr: ac.t01_corr,
        t01_cons: ac.t01_consumption,
        t01_loss: ac.t01_loss,
        t01_sum: ac.t01_sum,
        t02_curr: ac.t02_curr,
        t02_corr: ac.t02_corr,
        t02_cons: ac.t02_consumption,
        t02_loss: ac.t02_loss,
        t02_sum: ac.t02_sum,
        total_sum: Decimal.add(ac.t01_sum, ac.t02_sum)
      }
    end)
  end
end
