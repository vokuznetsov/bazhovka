defmodule Bazhovka.IndicationView do
  use Bazhovka.Web, :view

  defmodule Row do
    defstruct [:id, :address, :t01, :t02]
  end

  def rows(addresses_with_indications) do
    Enum.map(addresses_with_indications, fn(addr) ->
      indications = List.last addr.address_indications
      %Row{
        id: (if indications, do: indications.id),
        address: addr,
        t01: (if indications, do: indications.t01),
        t02: (if indications, do: indications.t02)
      }
    end)
  end
end
