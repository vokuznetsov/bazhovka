defmodule Bazhovka.PaymentReceiptView do
  use Bazhovka.Web, :view

  alias Bazhovka.Payment

  def due_date(%Payment{} = payment) do
    Payment.due_date(payment) |> Timex.format!("%d.%m.%Y", :strftime)
  end
end
