defmodule Bazhovka.AddressCorrection do
  use Bazhovka.Web, :model

  schema "address_corrections" do
    field :t01, :decimal
    field :t02, :decimal
    field :monthnum, :integer
    belongs_to :address, Bazhovka.Address

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:t01, :t02, :monthnum])
    |> validate_required([:t01, :t02, :monthnum])
  end
end
