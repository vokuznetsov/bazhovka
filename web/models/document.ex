defmodule Bazhovka.Document do
  use Bazhovka.Web, :model

  schema "documents" do
    field :name, :string
    field :path, :string
    field :upload, :any, virtual: true
    belongs_to :folder, Bazhovka.Folder

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :upload, :folder_id])
    |> save_file
    |> validate_required([:name, :path, :folder_id])
  end

  defp save_file(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{upload: upload}} ->
        new_path = Path.join(Application.get_env(:bazhovka, :documents_path), upload.filename)
        File.rename(upload.path, new_path)
        put_change(changeset, :path, new_path)
      _ ->
        changeset
    end
  end
end
