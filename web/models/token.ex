defmodule Bazhovka.Token do
  use Bazhovka.Web, :model

  alias Bazhovka.Repo

  @uid_size 48
  @ttl [days: 1]

  @primary_key {:uid, :string, []}
  schema "tokens" do
    field :value, :string
    field :expiration_time, :naive_datetime

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:uid, :value, :expiration_time])
    |> validate_required([:uid, :value, :expiration_time])
  end

  def create(value) do
    default_expiration_time = Timex.now |> Timex.shift(@ttl)
    create(value, default_expiration_time)
  end

  def create(value, expiration_time) do
    token = Repo.insert! %__MODULE__{uid: random_uid(), value: value, expiration_time: expiration_time}
    token.uid
  end

  def get(uid) do
    case Repo.get_by(__MODULE__, uid: uid) do
      %__MODULE__{value: value, expiration_time: expiration_time} ->
        if Timex.after?(expiration_time, Timex.now) do
          {:ok, value}
        else
          :notfound
        end
      _ ->
        :notfound
    end
  end

  defp random_uid do
    Base.url_encode64(:crypto.strong_rand_bytes(@uid_size))
  end
end
