defmodule Bazhovka.Poll do
  use Bazhovka.Web, :model

  @key_length 12
  @default_questions "согласен\nвоздержался\nне согласен"

  schema "polls" do
    field :title, :string
    field :questions, :map
    field :questions_string, :string, virtual: true
    field :active, :boolean, default: false
    has_many :votes, Bazhovka.Vote

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :questions_string, :active])
    |> normalize_questions
    |> activate_new
    |> validate_required([:title, :questions])
  end

  defp normalize_questions(%Ecto.Changeset{changes: %{questions_string: questions_string}} = changeset)
  when is_bitstring(questions_string) do
    questions_as_map =
      questions_string
      |> String.split("\n")
      |> Enum.filter(fn(s) -> !is_nil(s) && s != "" end)
      |> Enum.map(&String.trim_trailing/1)
      |> Enum.reduce(%{}, fn(q, acc) -> Map.put(acc, random_key(Enum.count(acc)), q) end)

    put_change(changeset, :questions, questions_as_map)
  end

  defp normalize_questions(%Ecto.Changeset{data: %{questions: questions}} = changeset)
  when is_map(questions) do
    put_change(changeset, :questions_string, Enum.join(Map.values(questions), "\n"))
  end

  defp normalize_questions(%Ecto.Changeset{} = changeset) do
    put_change(changeset, :questions_string, @default_questions)
  end

  defp activate_new(%Ecto.Changeset{data: %{id: nil}} = changeset) do
    put_change(changeset, :active, true)
  end

  defp activate_new(%Ecto.Changeset{} = changeset) do
    changeset
  end

  defp random_key(position) do
    rand_part = :crypto.strong_rand_bytes(@key_length)
    |> Base.url_encode64

    "#{position}__#{rand_part}"
  end
end
