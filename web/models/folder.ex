defmodule Bazhovka.Folder do
  use Bazhovka.Web, :model

  schema "folders" do
    field :name, :string
    belongs_to :parent, Bazhovka.Parent

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name])
    |> validate_required([:name])
  end
end
