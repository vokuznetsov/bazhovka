defmodule Bazhovka.User do
  use Bazhovka.Web, :model

  schema "users" do
    field :name, :string
    field :role, :string
    field :email, :string
    field :password, :string, virtual: true
    field :encrypted_password, :string
    field :send_electro_receipts, :boolean
    field :send_member_receipts, :boolean
    field :notice_allowed, :boolean
    many_to_many :addresses, Bazhovka.Address, join_through: "address_users"

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :role, :email, :encrypted_password, :password])
    |> downcase_email
    |> validate_required([:name, :email, :password])
    |> validate_length(:password, min: 6)
    |> unique_constraint(:email, message: "Email already taken")
    |> generate_encrypted_password
  end

  defp generate_encrypted_password(%Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset) do
    put_change(changeset, :encrypted_password, Comeonin.Bcrypt.hashpwsalt(password))
  end
  defp generate_encrypted_password(changeset), do: changeset

  defp downcase_email(%Ecto.Changeset{valid?: true, changes: %{email: email}} = changeset) do
    put_change(changeset, :email, String.downcase(email))
  end
  defp downcase_email(changeset), do: changeset
end
