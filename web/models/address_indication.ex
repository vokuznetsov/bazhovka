defmodule Bazhovka.AddressIndication do
  use Bazhovka.Web, :model

  schema "address_indications" do
    field :t01, :integer
    field :t02, :integer
    field :t01_corr, :integer
    field :t02_corr, :integer
    field :monthnum, :integer
    belongs_to :address, Bazhovka.Address

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:t01, :t01_corr, :t02, :t02_corr, :monthnum, :address_id])
    |> validate_required([:t01, :t02, :monthnum, :address_id])
  end
end
