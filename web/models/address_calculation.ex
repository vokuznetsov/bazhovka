defmodule Bazhovka.AddressCalculation do
  use Bazhovka.Web, :model

  schema "address_calculations" do
    field :t01_curr, :integer
    field :t01_prev, :integer
    field :t01_corr, :integer
    field :t02_curr, :integer
    field :t02_prev, :integer
    field :t02_corr, :integer
    field :t01_loss, :decimal
    field :t02_loss, :decimal
    field :t01_sum, :decimal
    field :t02_sum, :decimal
    field :t01_consumption, :integer
    field :t02_consumption, :integer
    field :t01_correction, :decimal
    field :t02_correction, :decimal
    belongs_to :address, Bazhovka.Address
    belongs_to :calculation, Bazhovka.Calculation
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:t01_curr, :t01_prev, :t01_corr, :t02_curr, :t02_prev, :t02_corr, :t01_loss, :t02_loss, :t01_sum, :t02_sum, :t01_consumption, :t02_consumption, :t01_correction, :t02_correction, :address_id])
    |> validate_required([:t01_curr, :t01_prev, :t02_curr, :t02_prev, :t01_loss, :t02_loss, :t01_sum, :t02_sum, :t01_consumption, :t02_consumption])
  end
end
