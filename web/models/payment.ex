defmodule Bazhovka.Payment do
  use Bazhovka.Web, :model

  alias Bazhovka.Repo
  alias Bazhovka.Month

  @types [
    member: "Членские взносы",
    lighting: "Освещение",
    electro: "Электроэнергия",
    target: "Целевые взносы",
    init: "Вступительные взносы"
  ]

  schema "payments" do
    field :sum, :decimal
    field :monthnum, :integer
    field :comment, :string
    field :type, :string
    field :made_at, :date
    belongs_to :address, Bazhovka.Address
    has_one :payment_receipt, Bazhovka.PaymentReceipt

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:sum, :monthnum, :comment, :address_id, :type, :made_at])
    |> validate_required([:sum, :monthnum, :address_id, :type])
  end

  def balance(%__MODULE__{} = payment) do
    from(
      p in __MODULE__,
      where: p.monthnum < ^payment.monthnum
        and p.address_id == ^payment.address_id
        and p.type == ^to_string(payment.type)
    ) |> Repo.aggregate(:sum, :sum) || Decimal.new(0)
  end

  def due_date(%__MODULE__{monthnum: monthnum}) do
    month = Month.from_num(monthnum)
    Timex.shift({month.year, month.month, 28}, months: 1)
  end

  def typename(%__MODULE__{type: type}) do
    types()[type]
  end

  def types do
    @types
  end
end
