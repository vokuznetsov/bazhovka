defmodule Bazhovka.Month do
  @derive {Phoenix.Param, key: :num}
  defstruct [:name, :num, :year, :month]

  @names %{
    1  => "Январь",
    2  => "Февраль",
    3  => "Март",
    4  => "Апрель",
    5  => "Май",
    6  => "Июнь",
    7  => "Июль",
    8  => "Август",
    9  => "Сентябрь",
    10 => "Октябрь",
    11 => "Ноябрь",
    12 => "Декабрь"
  }

  def new(year, month) when month > 0 and month <= 12 do
    %__MODULE__{
      name: "#{@names[month]} #{year}",
      num: year * 100 + month,
      year: year,
      month: month
    }
  end

  def current do
    {year, month, _} = Timex.today |> Timex.to_erl
    new(year, month)
  end

  def from_num(monthnum) do
    year = trunc(monthnum / 100)
    month = monthnum - year * 100
    new(year, month)
  end

  def next(month \\ current()) do
    {year, month, _} = Timex.shift({month.year, month.month, 1}, months: 1)
    new(year, month)
  end

  def prev(month \\ current()) do
    {year, month, _} = Timex.shift({month.year, month.month, 1}, months: -1)
    new(year, month)
  end
end
