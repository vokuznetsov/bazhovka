defmodule Bazhovka.Vote do
  use Bazhovka.Web, :model

  schema "votes" do
    field :answer, :string
    belongs_to :address, Bazhovka.Adress
    belongs_to :poll, Bazhovka.Poll

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:answer, :address_id, :poll_id])
    |> validate_required([:answer, :address_id, :poll_id])
  end

  defp bind_to_poll(changeset, %{"poll_id" => poll_id}) do
    build_assoc(changeset, :poll, %{id: poll_id})
  end
  defp bind_to_poll(changeset, _), do: changeset


end
