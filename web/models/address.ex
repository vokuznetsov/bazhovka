defmodule Bazhovka.Address do
  use Bazhovka.Web, :model

  schema "addresses" do
    field :name, :string
    field :account_num, :string
    field :meter_num, :string
    field :fider_num, :integer
    field :system, :boolean
    field :owner, :string
    field :is_free_member, :boolean, default: false
    field :is_member, :boolean
    field :email, :string
    field :pays_directly, :boolean
    many_to_many :users, Bazhovka.User, join_through: "address_users"
    has_many :address_indications, Bazhovka.AddressIndication

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :account_num, :meter_num, :fider_num, :system, :owner, :email, :is_free_member, :is_member])
    |> validate_required([:name, :is_member])
    |> unique_constraint(:name, message: "Address already taken")
  end
end
