defmodule Bazhovka.Calculation do
  use Bazhovka.Web, :model
  alias Bazhovka.Month

  schema "calculations" do
    field :monthnum, :integer
    field :f1t01curr, :integer
    field :f1t02curr, :integer
    field :f1t01prev, :integer
    field :f1t02prev, :integer
    field :f2t01curr, :integer
    field :f2t02curr, :integer
    field :f2t01prev, :integer
    field :f2t02prev, :integer
    field :lighting_t01_curr, :integer
    field :lighting_t02_curr, :integer
    field :lighting_t01_prev, :integer
    field :lighting_t02_prev, :integer
    field :entrance_t01_curr, :integer
    field :entrance_t02_curr, :integer
    field :entrance_t01_prev, :integer
    field :entrance_t02_prev, :integer
    field :multiple_factor, :integer
    field :loss_coeff, :decimal
    field :loss_const, :integer
    field :t01_rate, :decimal
    field :t02_rate, :decimal
    field :lighting_proportion, :decimal
    field :entrance_proportion, :decimal
    field :t01_consumption, :integer
    field :t02_consumption, :integer
    field :t01_loss_proportional, :integer
    field :t02_loss_proportional, :integer
    field :gen_t01_sum, :decimal
    field :gen_t02_sum, :decimal
    field :lighting_t01_consumption, :integer
    field :lighting_t02_consumption, :integer
    field :entrance_t01_consumption, :integer
    field :entrance_t02_consumption, :integer
    field :lighting_t01_sum, :decimal
    field :lighting_t02_sum, :decimal
    field :entrance_t01_sum, :decimal
    field :entrance_t02_sum, :decimal
    field :t01_loss, :integer
    field :t02_loss, :integer
    field :t01_loss_commercial, :integer
    field :t02_loss_commercial, :integer
    field :t01_counters_sum, :integer
    field :t02_counters_sum, :integer
    field :calculation_class, :string

    has_many :address_calculations, Bazhovka.AddressCalculation, on_delete: :delete_all

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:monthnum, :f1t01curr, :f1t02curr, :f1t01prev, :f1t02prev, :f2t01curr, :f2t02curr, :f2t01prev, :f2t02prev, :lighting_t01_curr, :lighting_t02_curr, :lighting_t01_prev, :lighting_t02_prev, :entrance_t01_curr, :entrance_t02_curr, :entrance_t01_prev, :entrance_t02_prev, :multiple_factor, :loss_coeff, :loss_const, :t01_rate, :t02_rate, :lighting_proportion, :entrance_proportion, :t01_consumption, :t02_consumption, :t01_loss_proportional, :t02_loss_proportional, :gen_t01_sum, :gen_t02_sum, :lighting_t01_consumption, :lighting_t02_consumption, :entrance_t01_consumption, :entrance_t02_consumption, :lighting_t01_sum, :lighting_t02_sum, :entrance_t01_sum, :entrance_t02_sum, :t01_loss, :t02_loss, :t01_counters_sum, :t02_counters_sum, :calculation_class, :t01_loss_commercial, :t02_loss_commercial])
    |> validate_required([:monthnum, :multiple_factor, :loss_coeff, :loss_const, :t01_rate, :t02_rate, :lighting_proportion, :entrance_proportion])
  end

  def due_date(calculation) do
    month = Month.from_num(calculation.monthnum)
    Timex.shift({month.year, month.month, 15}, months: 1)
  end
end
