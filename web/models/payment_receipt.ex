defmodule Bazhovka.PaymentReceipt do
  use Bazhovka.Web, :model

  schema "payment_receipts" do
    field :monthnum, :integer
    field :meta, :map
    field :balance, :decimal
    field :comment, :string
    field :type, :string
    field :note, :string
    belongs_to :address, Bazhovka.Address
    belongs_to :payment, Bazhovka.Payment

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:monthnum, :meta, :address_id, :payment_id, :balance, :comment, :type])
    |> validate_required([:monthnum, :meta, :address_id, :payment_id, :balance, :type])
  end
end
