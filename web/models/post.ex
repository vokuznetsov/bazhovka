defmodule Bazhovka.Post do
  use Bazhovka.Web, :model

  schema "posts" do
    field :caption, :string
    field :body, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:caption, :body])
    |> validate_required([:caption, :body])
  end
end
