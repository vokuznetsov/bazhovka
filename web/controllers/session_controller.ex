defmodule Bazhovka.SessionController do
  use Bazhovka.Web, :controller

  alias Bazhovka.Auth
  alias Bazhovka.User
  alias Bazhovka.Token

  def new(conn, %{"token" => token_id, "redirect_to" => redirect_to}) do
    with {:ok, user_id} <- Token.get(token_id),
         %User{} = user <- Repo.get(User, user_id),
         true <- user.role > "" do
      Auth.login(conn, user)
      |> redirect(to: redirect_to)
    else
      _ ->
        conn
        |> put_flash(:error, "Ссылка для входа недействительна.")
        |> redirect(to: page_path(conn, :index))
    end
  end

  def new(conn, params) do
    render(conn, "new.html", redirect_to: params["redirect_to"])
  end

  def create(conn, %{"data" => %{"email" => email, "password" => password}, "redirect_to" => redirect_to}) do
    redirect_addr = if redirect_to == "", do: page_path(conn, :index), else: redirect_to

    case Auth.login(conn, email, password) do
      {:ok, new_conn} ->
        new_conn
        |> put_flash(:info, "Вход выполнен.")
        |> redirect(to: redirect_addr)
      {:error, _reason, new_conn} ->
        new_conn
        |> put_flash(:error, "Hеправильный email или пароль.")
        |> render("new.html", redirect_to: redirect_to)
    end
  end

  def delete(conn, _params) do
    conn
    |> Auth.logout
    |> put_flash(:info, "Сессия завершена.")
    |> redirect(to: page_path(conn, :index))
  end
end
