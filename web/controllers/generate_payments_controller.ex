defmodule Bazhovka.GeneratePaymentsController do
  require Logger
  require Ecto.Query

  def private(calculation_id) do
    monthnum = 201902
    month = Bazhovka.Month.from_num(monthnum)
    light_t01_cons = 759
    light_t02_cons = 2293
    Bazhovka.GeneratePaymentsController.write_lighting_indication(month, light_t01_cons, light_t02_cons)
    Bazhovka.GeneratePaymentsController.write_fake_fider_indication(month)

    # create calculation
    # delete fake indications
    Bazhovka.GeneratePaymentsController.write_fider_avg_indication(month, 12)


    Bazhovka.PaymentService.create_payments(:member, month, 1500)
    Bazhovka.PaymentService.create_payments(:target, month, 0)

    calculation = Bazhovka.Repo.get(Bazhovka.Calculation, calculation_id) |> Bazhovka.Repo.preload(address_calculations: [[address: :users], :calculation])
    Bazhovka.PaymentService.create_payments(:electro, calculation)

    require Ecto.Query
    require Logger

    prsq = Ecto.Query.from(p in Bazhovka.PaymentReceipt,
      join: a in assoc(p, :address),
      where: p.monthnum == ^monthnum
      and not is_nil(a.email),
      # and p.type == "electro",
      preload: [:address])
    prs = Bazhovka.Repo.all(prsq)

    Enum.each(prs, fn(p) ->
      if p.address && (elem(Float.parse(p.meta["gen_sum"]), 0) != 0.0 || Decimal.cmp(p.balance, Decimal.new(0)) != :eq) &&
        (IO.gets("Отправить квитанцию #{p.type} - #{p.monthnum} - Начислено:#{p.meta["gen_sum"]}, Баланс:#{p.balance} для #{p.address.name} - #{p.address.owner} на #{p.address.email} y/n?\n") == "y\n") do

        case Bazhovka.Mailer.payment_receipt(p) |> Bazhovka.Mailer.deliver do
          {:ok, _mail} ->
            Logger.info("Sent #{p.type} payment_receipt for #{p.address.name} to #{p.address.email} success")
          other ->
            Logger.error("Sent #{p.type} payment_receipt for #{p.address.name} to #{p.address.email} error: #{inspect other}")
        end
      end
    end)
  end

  def write_lighting_indication(month, t01_cons, t02_cons) do
    address_id = 3
    write_consumption(month, address_id, t01_cons, t02_cons)
  end

  def write_fake_fider_indication(month) do
    t01_cons = 100
    t02_cons = 100
    f1_address_id = 1
    f2_address_id = 2
    write_consumption(month, f1_address_id, t01_cons, t02_cons)
    write_consumption(month, f2_address_id, t01_cons, t02_cons)
  end

  def write_consumption(month, address_id, t01_cons, t02_cons) do
    prev_ind_q = Ecto.Query.from(i in Bazhovka.AddressIndication,
      where: i.monthnum == ^Bazhovka.Month.prev(month).num and i.address_id == ^address_id)
    prev_ind = Bazhovka.Repo.one(prev_ind_q)

    write_indication(month, address_id, prev_ind.t01 + t01_cons, prev_ind.t02 + t02_cons)
  end

  def write_fider_avg_indication(month, number_of_month_for_avg_calculation) do
    avg_fider_indications = Bazhovka.AvgLoss.calculate_counters_value(month, number_of_month_for_avg_calculation)
    get = &(Keyword.get(avg_fider_indications, &1) |> round)
    Bazhovka.GeneratePaymentsController.write_indication(month, 1, get.(:f1t01), get.(:f1t02))
    Bazhovka.GeneratePaymentsController.write_indication(month, 2, get.(:f2t01), get.(:f2t02))
  end

  def write_indication(month, address_id, t01, t02) do
    changeset = Bazhovka.AddressIndication.changeset(
      %Bazhovka.AddressIndication{},
      %{monthnum: month.num, address_id: address_id, t01: t01, t02: t02})

    Bazhovka.Repo.insert!(changeset)
  end
end
