defmodule Bazhovka.PaymentController do
  use Bazhovka.Web, :controller

  import Ecto.Query, only: [from: 2]

  alias Bazhovka.Month
  alias Bazhovka.Address
  alias Bazhovka.Payment
  alias Bazhovka.PaymentReceipt
  alias Bazhovka.PaymentService

  defmodule Filter do
    defstruct [:month, :type, :with_archive]
  end

  def index(conn, params) do
    monthstr = get_in(params, ["filter", "month"])

    filter = %Filter{
      type: (get_in(params, ["filter", "type"]) || "all"),
      month: (if monthstr, do: Month.from_num(String.to_integer(monthstr)), else: Month.prev),
      with_archive: (get_in(params, ["filter", "with_archive"]) || "false")
    }

    payments = from(
      a in Address,
      left_join: p in Payment, on: p.address_id == a.id,

      select: %{
        address_id: a.id, address: a.name, owner: a.owner,
        made_at: fragment("max(case when monthnum = ? then made_at end)", ^filter.month.num),
        balance: fragment("sum(case when monthnum < ? then sum else 0.00 end)", ^filter.month.num),
        charges: fragment("-sum(case when monthnum = ? and sum < 0 then sum else 0.00 end)", ^filter.month.num),
        payments: fragment("sum(case when monthnum = ? and sum > 0 then sum else 0.00 end)", ^filter.month.num),
        total: fragment("sum(case when monthnum <= ? then sum else 0 end)", ^filter.month.num)
      },

      where: not a.system and (not a.archive or ^filter.with_archive == "true") and (^filter.type == "all" or p.type == ^filter.type),
      order_by: [a.owner, a.name],
      group_by: [a.id, a.name, a.owner]
    ) |> Repo.all

    month_total = from(
      p in Payment,

      select: %{
        balance: fragment("sum(case when monthnum < ? then sum else 0 end)", ^filter.month.num),
        charges: fragment("-sum(case when monthnum = ? and sum < 0 then sum else 0 end)", ^filter.month.num),
        payments: fragment("sum(case when monthnum = ? and sum > 0 then sum else 0 end)", ^filter.month.num),
        total: fragment("sum(case when monthnum <= ? then sum else 0 end)", ^filter.month.num)
      },

      where: ^filter.type == "all" or p.type == ^filter.type
    ) |> Repo.one

    render(conn, "index.html", payments: payments, month_total: month_total, filter: Map.from_struct(filter))
  end

  def new(conn, params) do
    month = case params["month"] do
              nil -> PaymentService.last_payment_month(params["address_id"], Month.prev())
              monthstr -> String.to_integer(monthstr) |> Month.from_num
            end

    address_id = params["address_id"]

    changeset = Payment.changeset(%Payment{}, %{monthnum: month.num, address_id: address_id})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"payment" => payment_params}) do
    changeset = Payment.changeset(%Payment{}, payment_params)

    case Repo.insert(changeset) do
      {:ok, payment} ->
        conn
        |> put_flash(:info, "Платёж записан.")
        |> redirect(to: payment_path(conn, :index, filter: %{month: payment.monthnum, type: payment.type}))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end
end
