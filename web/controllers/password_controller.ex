defmodule Bazhovka.PasswordController do
  use Bazhovka.Web, :controller

  alias Bazhovka.Auth
  alias Bazhovka.Repo
  alias Bazhovka.User
  alias Bazhovka.Token
  alias Bazhovka.Mailer

  def edit(conn, _params) do
    user = current_user(conn)
    changeset = User.changeset(user)
    render(conn, "edit.html", user: user, changeset: changeset)
  end

  def update(conn, %{"user" => %{"password" => password}}) do
    user = current_user(conn)
    changeset = User.changeset(user, %{password: password})

    case Repo.update(changeset) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "Пароль изменён.")
        |> redirect(to: page_path(conn, :index))
      {:error, changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end



  def reset(conn, _params) do
    case current_user(conn) do
      nil ->
        render(conn, "reset.html")
      %User{} = user ->
        changeset = User.changeset(user)
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end

  def token(conn, %{"user" => %{"email" => email}}) do
    case Repo.get_by(User, email: email) do
      %User{id: user_id, role: role} when role > "" ->
        Token.create(to_string(user_id)) |> Mailer.reset_password(email) |> Mailer.deliver
        conn
        |> put_flash(:info, "На адрес #{email} отправлена ссылка для сброса пароля")
        |> redirect(to: page_path(conn, :index))
      _ ->
        conn
        |> put_flash(:error, "Не удалось отправить ссылку для сброса пароля, вероятно пользователь с email-адресом #{email} не зарегистрирован в системе")
        |> redirect(to: password_path(conn, :reset))
    end
  end
end
