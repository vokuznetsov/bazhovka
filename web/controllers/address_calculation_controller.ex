defmodule Bazhovka.AddressCalculationController do
  use Bazhovka.Web, :controller
  import Ecto.Query, only: [from: 2]

  alias Bazhovka.Address
  alias Bazhovka.AddressCalculation

  def index(conn, %{"address_id" => address_id}) do
    address = Repo.get(Address, address_id)
    calculations = address_calculations(address)

    Bazhovka.AddressAccessHelpers.allow?(conn, address.id)
    |> render("index.html", address: address, address_calculations: calculations)
  end

  defp address_calculations(address) do
    from(ac in AddressCalculation,
      join: r in fragment("""
        SELECT ac.id, address_id, monthnum, row_number() OVER (PARTITION BY address_id, monthnum ORDER BY ac.id DESC)
        FROM address_calculations ac
        INNER JOIN calculations c ON ac.calculation_id = c.id
      """), on: [id: ac.id],
      where: ac.address_id == ^address.id and r.row_number == 1,
      preload: [:calculation],
      order_by: [desc: r.monthnum]
    ) |> Repo.all
  end
end
