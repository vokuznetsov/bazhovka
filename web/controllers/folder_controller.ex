defmodule Bazhovka.FolderController do
  use Bazhovka.Web, :controller

  alias Bazhovka.Folder

  def index(conn, _params) do
    folders = Repo.all(Folder)
    render(conn, "index.html", folders: folders)
  end

  def new(conn, _params) do
    changeset = Folder.changeset(%Folder{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"folder" => folder_params}) do
    changeset = Folder.changeset(%Folder{}, folder_params)

    case Repo.insert(changeset) do
      {:ok, _folder} ->
        conn
        |> put_flash(:info, "Папка создана.")
        |> redirect(to: folder_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    folder = Repo.get!(Folder, id)
    render(conn, "show.html", folder: folder)
  end

  def edit(conn, %{"id" => id}) do
    folder = Repo.get!(Folder, id)
    changeset = Folder.changeset(folder)
    render(conn, "edit.html", folder: folder, changeset: changeset)
  end

  def update(conn, %{"id" => id, "folder" => folder_params}) do
    folder = Repo.get!(Folder, id)
    changeset = Folder.changeset(folder, folder_params)

    case Repo.update(changeset) do
      {:ok, _folder} ->
        conn
        |> put_flash(:info, "Папка изменена.")
        |> redirect(to: folder_path(conn, :index))
      {:error, changeset} ->
        render(conn, "edit.html", folder: folder, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    folder = Repo.get!(Folder, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(folder)
    conn
    |> put_flash(:info, "Папка удалена.")
    |> redirect(to: folder_path(conn, :index))
  rescue Ecto.ConstraintError ->
      conn
      |> put_flash(:error, "Не удалось удалить папку, возможно она не пуста.")
      |> redirect(to: folder_document_path(conn, :index, id))
  end
end
