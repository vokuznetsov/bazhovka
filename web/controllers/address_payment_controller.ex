defmodule Bazhovka.AddressPaymentController do
  use Bazhovka.Web, :controller

  import Ecto.Query, only: [from: 2]

  alias Bazhovka.Month
  alias Bazhovka.Address
  alias Bazhovka.Payment
  alias Bazhovka.PaymentService

  def index(conn, params) do
    address = address(params)
    type_filter = get_in(params, ["filter", "type"]) || "all"
    address_payments = from(
      p in Payment,
      where: p.address_id == ^address.id and (p.type == ^type_filter or ^type_filter == "all"),
      order_by: [desc: p.monthnum, desc: p.inserted_at],
      preload: [:payment_receipt]
    ) |> Repo.all

    total_sum = from(
      p in Payment,
      select: [p.type, sum(p.sum)],
      where: p.address_id == ^address.id,
      group_by: p.type,
      order_by: p.type
    ) |> Repo.all

    Bazhovka.AddressAccessHelpers.allow?(conn, address.id)
    |> render("index.html", address_payments: address_payments, address: address, total_sum: total_sum)
  end

  def new(conn, params) do
    address = address(params)
    month = PaymentService.last_payment_month(address.id, Month.prev())
    changeset = Payment.changeset(%Payment{}, %{address_id: address.id, monthnum: month.num})
    render(conn, "new.html", changeset: changeset, address: address)
  end

  def create(conn, %{"payment" => payment_params} = params) do
    address = address(params)
    changeset = Payment.changeset(%Payment{}, payment_params)

    case Repo.insert(changeset) do
      {:ok, _address_payment} ->
        conn
        |> put_flash(:info, "Платёж записан.")
        |> redirect(to: address_address_payment_path(conn, :index, address))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset, address: address)
    end
  end

  def edit(conn, %{"id" => id} = params) do
    address = address(params)
    address_payment = Repo.get!(Payment, id)
    changeset = Payment.changeset(address_payment)
    render(conn, "edit.html", address_payment: address_payment, changeset: changeset, address: address)
  end

  def update(conn, %{"id" => id, "payment" => payment_params} = params) do
    address = address(params)
    address_payment = Repo.get!(Payment, id)
    changeset = Payment.changeset(address_payment, payment_params)

    case Repo.update(changeset) do
      {:ok, _address_payment} ->
        conn
        |> put_flash(:info, "Платёж изменён.")
        |> redirect(to: address_address_payment_path(conn, :index, address.id))
      {:error, changeset} ->
        render(conn, "edit.html", address_payment: address_payment, changeset: changeset, address: address)
    end
  end

  def delete(conn, %{"id" => id, "address_id" => address_id}) do
    address_payment = Repo.get!(Payment, id) |> Repo.preload(:payment_receipt)

    Repo.transaction fn ->
      # Here we use delete! (with a bang) because we expect
      # it to always work (and if it does not, it will raise).
      if address_payment.payment_receipt, do: Repo.delete!(address_payment.payment_receipt)
      Repo.delete!(address_payment)
    end

    conn
    |> put_flash(:info, "Платёж удалён.")
    |> redirect(to: address_address_payment_path(conn, :index, address_id))
  end

  def address(%{"address_id" => address_id}) do
    Repo.get(Address, address_id)
  end
end
