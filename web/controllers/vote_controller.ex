defmodule Bazhovka.VoteController do
  use Bazhovka.Web, :controller
  alias Bazhovka.{Vote, Poll, Repo, AddressService}

  def create(conn, %{"poll_id" => poll_id, "return_to" => return_to} = params) do
    poll = Repo.get!(Poll, poll_id)

    if poll.active do
      address = AddressService.user_address(current_user(conn))
      vote = Repo.get_by(Vote, address_id: address.id, poll_id: poll_id) || %Vote{}

      params = Map.put(params, "address_id", address.id)
      changeset = Vote.changeset(vote, params)

      case Repo.insert_or_update(changeset) do
        {:ok, _} ->
          conn
          |> put_flash(:info, "Спасибо, ваш ответ записан.")
          |> redirect(to: return_to)
        {:error, _} ->
          conn
          |> put_flash(:error, "Ошибка, не удалось записать ответ.")
          |> redirect(to: return_to)
      end
    else
      conn
      |> put_flash(:error, "Опрос завершён. Голосовать больше нельзя.")
      |> redirect(to: return_to)
    end
  end
end