defmodule Bazhovka.PersonalController do
  use Bazhovka.Web, :controller

  alias Bazhovka.Repo

  def index(conn, _params) do
    user = current_user(conn)
    |> Repo.preload(:addresses)

    addresses = if user, do: user.addresses, else: []

    render(conn, "index.html", addresses: addresses, user: user)
  end
end
