defmodule Bazhovka.IndicationController do
  use Bazhovka.Web, :controller
  import Ecto.Query, only: [from: 2]

  alias Bazhovka.Month
  alias Bazhovka.Address
  alias Bazhovka.AddressIndication

  def index(conn, %{"month" => monthstr}) do
    monthnum = String.to_integer(monthstr)
    month = Month.from_num(monthnum)
    render(conn, "index.html", addresses_with_indications: addresses_with_indications(month.num), month: month)
  end

  def index(conn, _params) do
    month = Month.prev
    render(conn, "index.html", addresses_with_indications: addresses_with_indications(month.num), month: month)
  end

  def new(conn, %{"address" => address_id, "month" => monthstr}) do
    monthnum = String.to_integer(monthstr)
    month = Month.from_num(monthnum)
    address = Repo.get(Address, address_id)

    address_indication =
      Repo.get_by(AddressIndication, address_id: address_id, monthnum: month.num)
      |> Repo.preload(:address)

    if address_indication do
      changeset = AddressIndication.changeset(address_indication)
      Bazhovka.AddressAccessHelpers.allow?(conn, address.id)
      |> render("edit.html", changeset: changeset, prev_indications: prev_indications(monthnum, address_id), address_indication: address_indication)
    else
      changeset = AddressIndication.changeset(%AddressIndication{address: address, address_id: address_id, monthnum: month.num})
      Bazhovka.AddressAccessHelpers.allow?(conn, address.id)
      |> render("new.html", changeset: changeset, prev_indications: prev_indications(monthnum, address_id))
    end
  end

  def create(conn, %{"address_indication" => indication_params, "address" => address_id, "month" => monthstr}) do
    monthnum = String.to_integer(monthstr)
    month = Month.from_num(monthnum)
    address = Repo.get(Address, address_id)

    Bazhovka.AddressAccessHelpers.allow?(conn, address.id)

    changeset = AddressIndication.changeset(%AddressIndication{address: address,
                                                               address_id: address.id,
                                                               monthnum: month.num},
                                            indication_params)

    case Repo.insert(changeset) do
      {:ok, _indication} ->
        conn
        |> put_flash(:info, "Показания записаны.")
        |> redirect(to: indications_history_path(conn, :index, address: address))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset, prev_indications: prev_indications(monthnum, address_id))
    end
  end

  def update(conn, %{"id" => id, "address_indication" => indication_params, "address" => address_id, "month" => monthstr}) do
    monthnum = String.to_integer(monthstr)
    month = Month.from_num(monthnum)
    address = Repo.get(Address, address_id)

    Bazhovka.AddressAccessHelpers.allow?(conn, address.id)

    address_indication =
      Repo.get_by(AddressIndication, address_id: address_id, monthnum: month.num)
      |> Repo.preload(:address)

    changeset = AddressIndication.changeset(address_indication, indication_params)

    case Repo.update(changeset) do
      {:ok, _indication} ->
        conn
        |> put_flash(:info, "Показания записаны.")
        |> redirect(to: indications_history_path(conn, :index, address: address))
      {:error, changeset} ->
        render(conn, "edit.html", changeset: changeset,
                                    prev_indications: prev_indications(monthnum, address_id),
                                    address_indication: address_indication)
    end
  end

  def delete(conn, %{"id" => id}) do
    indication = Repo.get!(AddressIndication, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(indication)

    conn
    |> put_flash(:info, "Показания удалены.")
    |> redirect(to: indication_path(conn, :index, month: indication.monthnum))
  end

  def batch(conn, %{"month" => monthstr}) do
    monthnum = String.to_integer(monthstr)
    month = Month.from_num(monthnum)

    previous_indications = addresses_with_indications(Month.prev(month).num) |> Enum.group_by(&(&1.id))
    current_indications = addresses_with_indications(month.num) |> Enum.group_by(&(&1.id))

    addresses_with_indications = Enum.reduce(previous_indications, Map.new, fn({address_id, [prev_ind]}, map) ->
      [curr_ind] = current_indications[address_id]

      address_indications =  %{name: prev_ind.name, owner: prev_ind.owner}

      address_indications = case curr_ind.address_indications do
                              [ind] -> Map.put_new(address_indications, :current, %{t01: ind.t01, t02: ind.t02})
                              _ -> Map.put_new(address_indications, :current, %{t01: nil, t02: nil})
                            end

      address_indications = case prev_ind.address_indications do
                              [ind] -> Map.put_new(address_indications, :prev, %{t01: ind.t01, t02: ind.t02})
                              _ ->  Map.put_new(address_indications, :prev, %{t01: nil, t02: nil})
                            end

      Map.put_new(map, address_id, address_indications)
    end)
    |> Enum.sort(fn({id1, ai1}, {id2, ai2}) -> ai1.owner < ai2.owner or ai1.owner == ai2.owner and id1 < id2 end)

    render(conn, "batch.html", addresses_with_indications: addresses_with_indications, month: month)
  end

  def save_batch(conn, %{"month" => monthstr, "address_indications" => address_indications}) do
    month = String.to_integer(monthstr) |> Month.from_num

    Repo.transaction(fn ->
      existing_indications = from(i in AddressIndication, where: i.monthnum == ^month.num, preload: [:address]) |> Repo.all |> Enum.group_by(&(&1.address_id))
      Enum.each(address_indications, fn({address_id_str, %{"t01" => t01str, "t02" => t02str}}) ->
        address_id = String.to_integer(address_id_str)
        if t01str != "" and t02str != "" do
          indications = %{t01: String.to_integer(t01str), t02: String.to_integer(t02str)}

          case existing_indications[address_id] do
            nil ->
              AddressIndication.changeset(%AddressIndication{address_id: address_id, monthnum: month.num}, indications)
              |> Repo.insert!
            [ind] ->
              AddressIndication.changeset(ind, indications)
              |> Repo.update!
          end
        end
      end)
    end)

    conn
    |> put_flash(:info, "Показания сохранены.")
    |> redirect(to: indication_path(conn, :batch, month: monthstr))
  end

  defp addresses_with_indications(monthnum) do
    from(a in Address,
      left_join: i in assoc(a, :address_indications), on: i.monthnum == ^monthnum,
      order_by: [a.owner, a.name],
      preload: [address_indications: i]
    ) |> Repo.all
  end

  defp prev_indications(monthnum, address_id) do
    from(i in AddressIndication,
      where: i.monthnum < ^monthnum and i.address_id == ^address_id,
      order_by: [desc: :monthnum, desc: :id],
      limit: 1
    ) |> Repo.one
  end
end
