defmodule Bazhovka.IndicationsHistoryController do
  use Bazhovka.Web, :controller

  import Ecto.Query, only: [from: 2]

  alias Bazhovka.Repo
  alias Bazhovka.Address
  alias Bazhovka.AddressIndication

  def index(conn, %{"address" => address_id}) do
    address = Repo.get(Address, address_id)

    Bazhovka.AddressAccessHelpers.allow?(conn, address.id)

    indications = from(i in AddressIndication, where: i.address_id == ^address_id, order_by: [desc: i.monthnum])
    |> Repo.all

    render(conn, "index.html", indications: indications, address: address)
  end
end
