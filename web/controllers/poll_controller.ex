defmodule Bazhovka.PollController do
  use Bazhovka.Web, :controller
  import Ecto.Query, only: [from: 2]

  alias Bazhovka.Poll
  alias Bazhovka.PollService

  def index(conn, _params) do
    polls = PollService.load_polls()
    render(conn, "index.html", polls: polls)
  end

  def new(conn, _params) do
    changeset = Poll.changeset(%Poll{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"poll" => poll_params}) do
    changeset = Poll.changeset(%Poll{}, poll_params)

    case Repo.insert(changeset) do
      {:ok, _poll} ->
        conn
        |> put_flash(:info, "Опрос создан.")
        |> redirect(to: poll_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    poll = Repo.get!(Poll, id)
    changeset = Poll.changeset(poll)
    render(conn, "edit.html", poll: poll, changeset: changeset)
  end

  def update(conn, %{"id" => id, "poll" => poll_params}) do
    poll = Repo.get!(Poll, id)
    changeset = Poll.changeset(poll, poll_params)

    case Repo.update(changeset) do
      {:ok, poll} ->
        conn
        |> put_flash(:info, "Опрос обновлён.")
        |> redirect(to: poll_path(conn, :index))
      {:error, changeset} ->
        render(conn, "edit.html", poll: poll, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    poll = Repo.get!(Poll, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(poll)

    conn
    |> put_flash(:info, "Опрос удалён.")
    |> redirect(to: poll_path(conn, :index))
  end

  def complete(conn, %{"id" => id}) do
    Repo.get!(Poll, id)
    |> Poll.changeset(%{"active" => false})
    |> Repo.update!

    conn
    |> put_flash(:info, "Опрос завершён.")
    |> redirect(to: poll_path(conn, :index))
  end

  def resume(conn, %{"id" => id}) do
    Repo.get!(Poll, id)
    |> Poll.changeset(%{"active" => true})
    |> Repo.update!

    conn
    |> put_flash(:info, "Опрос возобновлён.")
    |> redirect(to: poll_path(conn, :index))
  end
end
