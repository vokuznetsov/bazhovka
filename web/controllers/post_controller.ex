defmodule Bazhovka.PostController do
  use Bazhovka.Web, :controller

  alias Bazhovka.Post
  alias Bazhovka.User
  alias Bazhovka.Token
  alias Bazhovka.Mailer

  require Logger

  def new(conn, _params) do
    changeset = Post.changeset(%Post{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"post" => post_params, "opts" => opts}) do
    changeset = Post.changeset(%Post{}, post_params)

    case Repo.insert(changeset) do
      {:ok, post} ->
        if opts["notify"] == "true", do: notify_users(post)

        conn
        |> put_flash(:info, "Новость создана.")
        |> redirect(to: post_path(conn, :show, post))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    post = Repo.get!(Post, id)
    render(conn, "show.html", post: post)
  end

  def edit(conn, %{"id" => id}) do
    post = Repo.get!(Post, id)
    changeset = Post.changeset(post)
    render(conn, "edit.html", post: post, changeset: changeset)
  end

  def update(conn, %{"id" => id, "post" => post_params, "opts" => opts}) do
    post = Repo.get!(Post, id)
    changeset = Post.changeset(post, post_params)

    case Repo.update(changeset) do
      {:ok, post} ->
        if opts["notify"] == "true", do: notify_users(post)

        conn
        |> put_flash(:info, "Новость изменена.")
        |> redirect(to: post_path(conn, :show, post))
      {:error, changeset} ->
        render(conn, "edit.html", post: post, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    post = Repo.get!(Post, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(post)

    conn
    |> put_flash(:info, "Новость удалена.")
    |> redirect(to: page_path(conn, :index))
  end

  defp notify_users(post) do
    Task.async fn ->
      from(u in User, where: u.notice_allowed and not is_nil(u.email))
      |> Repo.all
      |> Enum.each(fn(user) ->
        token_id = Token.create(to_string(user.id), Timex.now |> Timex.shift([weeks: 1]))
        case Mailer.news(token_id, post, user.email) |> Mailer.deliver do
          {:ok, _mail} ->
            Logger.info("Sent post(#{post.id}) notification to #{user.email} success")
          other ->
            Logger.error("Sent post(#{post.id}) notification to #{user.email} error: #{inspect other}")
        end
      end)
    end
  end
end
