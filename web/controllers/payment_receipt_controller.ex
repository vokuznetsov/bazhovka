defmodule Bazhovka.PaymentReceiptController do
  use Bazhovka.Web, :controller

  alias Bazhovka.PaymentReceipt

  import Ecto.Query, only: [from: 2]

  plug :put_layout, false

  def show(conn, %{"id" => id}) do
    payment_receipt = from(
      pr in PaymentReceipt,
      where: pr.id == ^id,
      preload: [:address, :payment]
    ) |> Repo.one!

    conn
    |> Bazhovka.AddressAccessHelpers.allow?(payment_receipt.address_id)
    |> render("#{payment_receipt.payment.type}.html", payment_receipt: payment_receipt)
  end
end
