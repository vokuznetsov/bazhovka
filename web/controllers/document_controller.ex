defmodule Bazhovka.DocumentController do
  use Bazhovka.Web, :controller

  alias Bazhovka.Folder
  alias Bazhovka.Document

  def index(conn, %{"folder_id" => folder_id}) do
    folder = Repo.get!(Folder, folder_id)
    documents = Repo.all(from(d in Document, where: d.folder_id == ^folder_id, order_by: d.name))
    render(conn, "index.html", documents: documents, folder: folder)
  end

  def index(conn, _params) do
    documents = Repo.all(from(d in Document, order_by: [desc: :id]))
    render(conn, "index.html", documents: documents, folder: nil)
  end

  def new(conn, params) do
    changeset = Document.changeset(%Document{folder_id: params["folder_id"]})
    render(conn, "new.html", changeset: changeset, folder_id: params["folder_id"])
  end

  def create(conn, %{"document" => document_params}) do
    changeset = Document.changeset(%Document{}, document_params)

    case Repo.insert(changeset) do
      {:ok, document} ->
        conn
        |> put_flash(:info, "Документ создан.")
        |> redirect(to: folder_document_path(conn, :index, document.folder_id))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    document = Repo.get!(Document, id)
    filename = Path.basename(document.path)
    escaped_filename = URI.encode(filename)

    conn
    |> put_resp_content_type("application/octet-stream", nil)
    |> put_resp_header("content-disposition", ~s[attachment; filename*=UTF-8''#{escaped_filename}])
    |> send_file(:ok, document.path)
  end

  def edit(conn, %{"id" => id}) do
    document = Repo.get!(Document, id)
    changeset = Document.changeset(document)
    render(conn, "edit.html", document: document, changeset: changeset)
  end

  def update(conn, %{"id" => id, "document" => document_params}) do
    document = Repo.get!(Document, id)
    changeset = Document.changeset(document, document_params)

    case Repo.update(changeset) do
      {:ok, document} ->
        conn
        |> put_flash(:info, "Документ изменён.")
        |> redirect(to: folder_document_path(conn, :index, document.folder_id))
      {:error, changeset} ->
        render(conn, "edit.html", document: document, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    document = Repo.get!(Document, id)
    path = document.path

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(document)
    File.rm(path)

    conn
    |> put_flash(:info, "Документ удалён.")
    |> redirect(to: document_path(conn, :index))
  end
end
