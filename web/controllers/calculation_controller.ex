defmodule Bazhovka.CalculationController do
  use Bazhovka.Web, :controller
  import Ecto.Query, only: [from: 2]

  alias Bazhovka.Repo
  alias Bazhovka.Month
  alias Bazhovka.Calculation
  alias Bazhovka.Calculations.Version6, as: Calculator

  def index(conn, _params) do
    calculation_groups = from(c in Calculation, order_by: [desc: :inserted_at])
    |> Repo.all
    |> Enum.group_by(&(&1.monthnum))
    |> Enum.map(fn {monthnum, calculations} -> {Month.from_num(monthnum), calculations} end)
    |> Enum.sort(fn({month1, _calculations1}, {month2, _calculations2}) -> month1.num >= month2.num end)

    render(conn, "index.html", calculation_groups: calculation_groups)
  end

  def new(conn, _params) do
    prev_calculation = from(c in Calculation, order_by: [desc: :id], limit: 1) |> Repo.one
    new_calculation = case prev_calculation do
      %Calculation{
        monthnum: monthnum,
        t01_rate: t01_rate, t02_rate: t02_rate,
        multiple_factor: multiple_factor, loss_coeff: loss_coeff, loss_const: loss_const,
        lighting_proportion: lighting_proportion, entrance_proportion: entrance_proportion
      } ->
        %Calculation{
          monthnum: monthnum,
          t01_rate: t01_rate, t02_rate: t02_rate,
          multiple_factor: multiple_factor, loss_coeff: loss_coeff, loss_const: loss_const,
          lighting_proportion: lighting_proportion, entrance_proportion: entrance_proportion
        }
      nil -> %Calculation{}
    end

    changeset = Calculation.changeset(new_calculation)
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"calculation" => calculation_params}) do
    changeset = Calculation.changeset(%Calculation{}, calculation_params)

    with {:ok, calculation1} <- Repo.insert(changeset),
         calculation2 <- Repo.preload(calculation1, :address_calculations),
         calculation3 <- Calculator.calculate(calculation2),
         {:ok, calculation4} <- Repo.update(calculation3)
    do
      conn
      |> put_flash(:info, "Расчёт выполнен и сохранён.")
      |> redirect(to: calculation_path(conn, :show, calculation4))
    else
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    calculation = Repo.get!(Calculation, id) |> Repo.preload(address_calculations: :address)
    render(conn, "show.html", calculation: calculation)
  end

  def delete(conn, %{"id" => id}) do
    calculation = Repo.get!(Calculation, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(calculation)

    conn
    |> put_flash(:info, "Расчёт удалён.")
    |> redirect(to: calculation_path(conn, :index))
  end
end
