defmodule Bazhovka.AddressController do
  use Bazhovka.Web, :controller
  import Ecto.Query, only: [from: 2]

  alias Bazhovka.Address

  def index(conn, _params) do
    addresses = from(a in Address, order_by: [a.owner, a.name]) |> Repo.all()
    render(conn, "index.html", addresses: addresses)
  end

  def edit(conn, %{"id" => id}) do
    address = Repo.get(Address, id)
    changeset = Address.changeset(address)
    render(conn, "edit.html", address: address, changeset: changeset)
  end

  def update(conn, %{"id" => id, "address" => address_params}) do
    address = Repo.get!(Address, id)
    changeset = Address.changeset(address, address_params)

    case Repo.update(changeset) do
      {:ok, _address} ->
        conn
        |> put_flash(:info, "Данные адреса изменены.")
        |> redirect(to: address_path(conn, :index))
      {:error, changeset} ->
        render(conn, "edit.html", address: address, changeset: changeset)
    end
  end
end
