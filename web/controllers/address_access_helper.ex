defmodule Bazhovka.AddressAccessHelpers do
  import Plug.Conn
  import Phoenix.Controller
  import Ecto.Query, only: [from: 2]

  alias Bazhovka.Repo
  alias Bazhovka.Address

  def allow?(conn, address_id) do
    user = Bazhovka.Auth.Helpers.current_user(conn)

    if user do
      if user.role == "admin" do
        conn
      else
        if user_address(user, address_id) do
          conn
        else
          forbidden(conn)
        end
      end
    else
      forbidden(conn)
    end
  end

  defp forbidden(conn) do
    conn
    |> put_status(:forbidden)
    |> render(Bazhovka.ErrorView, "403.html")
    |> halt
  end

  defp user_address(user, address_id) do
    from(
      a in Address,
      join: (au in "address_users"), on: au.user_id == ^user.id,
      where: au.address_id == ^address_id,
      select: 1,
      limit: 1
    ) |> Repo.one
  end
end
