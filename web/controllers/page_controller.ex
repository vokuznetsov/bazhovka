defmodule Bazhovka.PageController do
  use Bazhovka.Web, :controller

  alias Bazhovka.Post

  def index(conn, _params) do
    posts = if current_user(conn) do
      from(p in Post, order_by: [desc: :id]) |> Repo.all
    else
      []
    end

    render conn, "index.html", posts: posts
  end
end
