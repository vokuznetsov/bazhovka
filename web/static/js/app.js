// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"


$("#payment_type").on("change", update_payment_type);

if ($("#payment_made_at").val() == "") {
  $("#payment_made_at").val(new Date().toISOString().split('T')[0]);
}

function update_payment_type() {
  switch ($("#payment_type").val()) {
    case "member":
      $("#payment_comment").val("Оплата членских взносов");
      $('#payment_sum').val(1500);
      break;
    case "electro":
      $("#payment_comment").val("Оплата электроэнергии");
      $('#payment_sum').val("");
      break;
    case "lighting":
      $("#payment_comment").val("Оплата за освещение");
      $('#payment_sum').val("");
      break;
    case "target":
      $("#payment_comment").val("Оплата целевых взносов");
      $('#payment_sum').val("");
      break;
    case "init":
      $("#payment_comment").val("Оплата вступительных взносов");
      $('#payment_sum').val(5000);
      break;
  }
}

update_payment_type();