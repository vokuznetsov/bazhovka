defmodule Bazhovka.Router do
  use Bazhovka.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Bazhovka.Auth
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Bazhovka do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index

    resources "/payment_receipts", PaymentReceiptController, only: [:show]

    resources "/addresses", AddressController, only: [:index, :edit, :update] do
      resources "/calculations", AddressCalculationController, only: [:index]
      resources "/payments", AddressPaymentController, except: [:show]
    end

    resources "/indications", IndicationController, only: [:index, :new, :create, :delete, :update]
    get "/indications/history", IndicationsHistoryController, :index
    get "/indications/batch", IndicationController, :batch
    post "/indications/batch", IndicationController, :save_batch

    resources "/calculations", CalculationController, except: [:edit, :update]
    resources "/payments", PaymentController, only: [:index, :new, :create]
    resources "/users", UserController, except: [:show]

    get "/login", SessionController, :new
    post "/login", SessionController, :create
    get "/logout", SessionController, :delete

    get "/password/reset", PasswordController, :reset
    post "/password/reset", PasswordController, :token
    get "/password/edit", PasswordController, :edit
    put "/password", PasswordController, :update

    resources "/posts", PostController, except: [:index]
    resources "/my", PersonalController, only: [:index]

    resources "/documents", DocumentController

    resources "/folders", FolderController, except: [:show] do
      get "/", DocumentController, :index
    end

    resources "/polls", PollController, except: [:show] do
      post "/vote", VoteController, :create
    end
    post "/polls/:id/complete", PollController, :complete
    post "/polls/:id/resume", PollController, :resume
  end

  # Other scopes may use custom stacks.
  # scope "/api", Bazhovka do
  #   pipe_through :api
  # end
end
