defmodule Bazhovka.Calculations.Version6 do
  import Ecto.Query, only: [from: 2]

  alias Bazhovka.Repo
  alias Bazhovka.Month
  alias Bazhovka.Address
  alias Bazhovka.Calculation
  alias Bazhovka.AddressIndication

  alias Decimal, as: D

  @techical_loss_max_ratio D.new(0.1)

  def calculate(calculation) do
    fider1 = Repo.get_by(Address, name: "Фидер 1")
    fider2 = Repo.get_by(Address, name: "Фидер 2")
    lighting = Repo.get_by(Address, name: "Уличное освещение")
    entrance = Repo.get_by(Address, name: "Въездные ворота")

    current_indications = current_indications(calculation)
    previous_indications = previous_indications(calculation)

    f1t01curr = current_indications[fider1.id].t01
    f1t02curr = current_indications[fider1.id].t02
    f1t01prev = previous_indications[fider1.id].t01
    f1t02prev = previous_indications[fider1.id].t02

    f2t01curr = current_indications[fider2.id].t01
    f2t02curr = current_indications[fider2.id].t02
    f2t01prev = previous_indications[fider2.id].t01
    f2t02prev = previous_indications[fider2.id].t02

    lighting_t01_curr = current_indications[lighting.id].t01
    lighting_t01_prev = previous_indications[lighting.id].t01
    lighting_t01_consumption = D.mult(D.new(lighting_t01_curr - lighting_t01_prev), D.new(calculation.lighting_proportion)) |> D.round |> D.to_integer
    lighting_t01_sum = D.mult(D.new(lighting_t01_consumption), D.new(calculation.t01_rate)) |> D.round(2)

    lighting_t02_curr = current_indications[lighting.id].t02
    lighting_t02_prev = previous_indications[lighting.id].t02
    lighting_t02_consumption = D.mult(D.new(lighting_t02_curr - lighting_t02_prev), D.new(calculation.lighting_proportion)) |> D.round |> D.to_integer
    lighting_t02_sum = D.mult(D.new(lighting_t02_consumption), D.new(calculation.t02_rate)) |> D.round(2)

    entrance_t01_curr = current_indications[entrance.id].t01
    entrance_t01_prev = previous_indications[entrance.id].t01
    entrance_t01_consumption = D.mult(D.new(entrance_t01_curr - entrance_t01_prev), D.new(calculation.entrance_proportion)) |> D.round |> D.to_integer
    entrance_t01_sum = D.mult(D.new(entrance_t01_consumption), D.new(calculation.t01_rate)) |> D.round(2)

    entrance_t02_curr = current_indications[entrance.id].t02
    entrance_t02_prev = previous_indications[entrance.id].t02
    entrance_t02_consumption = D.mult(D.new(entrance_t02_curr - entrance_t02_prev), D.new(calculation.entrance_proportion)) |> D.round |> D.to_integer
    entrance_t02_sum = D.mult(D.new(entrance_t02_consumption), D.new(calculation.t02_rate)) |> D.round(2)

    t01_consumption = D.add(D.mult(D.new(f1t01curr - f1t01prev), D.new(calculation.multiple_factor)), D.mult(D.new(f2t01curr - f2t01prev), D.new(calculation.multiple_factor))) |> D.round |> D.to_integer
    t02_consumption = D.add(D.mult(D.new(f1t02curr - f1t02prev), D.new(calculation.multiple_factor)), D.mult(D.new(f2t02curr - f2t02prev), D.new(calculation.multiple_factor))) |> D.round |> D.to_integer

    t01_loss_proportional = D.mult(D.new(t01_consumption), D.new(calculation.loss_coeff)) |> D.div(D.new(100)) |> D.round |> D.to_integer
    t02_loss_proportional = D.mult(D.new(t02_consumption), D.new(calculation.loss_coeff)) |> D.div(D.new(100)) |> D.round |> D.to_integer

    gen_t01_sum = D.mult(D.new(t01_consumption + t01_loss_proportional + calculation.loss_const), D.new(calculation.t01_rate)) |> D.round(2)
    gen_t02_sum = D.mult(D.new(t02_consumption + t02_loss_proportional + calculation.loss_const), D.new(calculation.t02_rate)) |> D.round(2)

    t01_counters_sum = t01_counters_sum(current_indications, previous_indications)
    t02_counters_sum = t02_counters_sum(current_indications, previous_indications)

    t01_loss = t01_consumption + t01_loss_proportional + calculation.loss_const - t01_counters_sum
    t02_loss = t02_consumption + t02_loss_proportional + calculation.loss_const - t02_counters_sum

    t01_loss_technical = Enum.min([t01_loss, D.to_integer(D.mult(D.new(t01_consumption), @techical_loss_max_ratio))])
    t02_loss_technical = Enum.min([t02_loss, D.to_integer(D.mult(D.new(t02_consumption), @techical_loss_max_ratio))])

    t01_loss_commercial = t01_loss - t01_loss_technical
    t02_loss_commercial = t02_loss - t02_loss_technical

    address_calculations = Enum.map(personal_addresses(), fn(address) ->
      a_t01_curr = current_indications[address.id].t01
      a_t01_prev = previous_indications[address.id].t01
      a_t02_curr = current_indications[address.id].t02
      a_t02_prev = previous_indications[address.id].t02

      a_t01_corr = current_indications[address.id].t01_corr
      a_t02_corr = current_indications[address.id].t02_corr

      a_t01_consumption = a_t01_curr - a_t01_prev - (a_t01_corr || 0)
      a_t02_consumption = a_t02_curr - a_t02_prev - (a_t02_corr || 0)

      a_t01_coeff = D.div(D.new(a_t01_consumption), D.new(t01_counters_sum))
      a_t02_coeff = D.div(D.new(a_t02_consumption), D.new(t02_counters_sum))

      a_t01_loss = D.mult(a_t01_coeff, D.new(t01_loss_technical))
      a_t02_loss = D.mult(a_t02_coeff, D.new(t02_loss_technical))

      [a_t01_paid_consumption, a_t02_paid_consumption] = if address.pays_directly do
        [a_t01_loss, a_t02_loss]
      else
        [ D.add(D.new(a_t01_consumption), a_t01_loss),
          D.add(D.new(a_t02_consumption), a_t02_loss) ]
      end

      a_t01_sum = D.mult(a_t01_paid_consumption, D.new(calculation.t01_rate)) |> D.round(2)
      a_t02_sum = D.mult(a_t02_paid_consumption, D.new(calculation.t02_rate)) |> D.round(2)

      %{
        address_id: address.id,
        t01_curr: a_t01_curr, t01_prev: a_t01_prev, t02_curr: a_t02_curr, t02_prev: a_t02_prev,
        t01_consumption: a_t01_consumption, t02_consumption: a_t02_consumption,
        t01_loss: a_t01_loss, t02_loss: a_t02_loss,
        t01_sum: a_t01_sum, t02_sum: a_t02_sum,
        t01_corr: a_t01_corr, t02_corr: a_t02_corr
      }
    end)

    Calculation.changeset(calculation, %{
      f1t01curr: f1t01curr, f1t02curr: f1t02curr, f1t01prev: f1t01prev, f1t02prev: f1t02prev,
      f2t01curr: f2t01curr, f2t02curr: f2t02curr, f2t01prev: f2t01prev, f2t02prev: f2t02prev,

      lighting_t01_curr: lighting_t01_curr, lighting_t01_prev: lighting_t01_prev,
      lighting_t01_consumption: lighting_t01_consumption, lighting_t01_sum: lighting_t01_sum,
      lighting_t02_curr: lighting_t02_curr, lighting_t02_prev: lighting_t02_prev,
      lighting_t02_consumption: lighting_t02_consumption, lighting_t02_sum: lighting_t02_sum,

      entrance_t01_curr: entrance_t01_curr, entrance_t01_prev: entrance_t01_prev,
      entrance_t01_consumption: entrance_t01_consumption, entrance_t01_sum: entrance_t01_sum,
      entrance_t02_curr: entrance_t02_curr, entrance_t02_prev: entrance_t02_prev,
      entrance_t02_consumption: entrance_t02_consumption, entrance_t02_sum: entrance_t02_sum,

      t01_consumption: t01_consumption, t02_consumption: t02_consumption,
      t01_loss_proportional: t01_loss_proportional, t02_loss_proportional: t02_loss_proportional,

      gen_t01_sum: gen_t01_sum, gen_t02_sum: gen_t02_sum,
      t01_counters_sum: t01_counters_sum, t02_counters_sum: t02_counters_sum,
      t01_loss: t01_loss, t02_loss: t02_loss,
      t01_loss_commercial: t01_loss_commercial, t02_loss_commercial: t02_loss_commercial,

      address_calculations: address_calculations,

      calculation_class: inspect(__MODULE__)
    })
    |> Ecto.Changeset.cast_assoc(:address_calculations, address_calculations)
  end

  def current_indications(calculation) do
    indications_by_address_id(calculation.monthnum)
  end

  def previous_indications(calculation) do
    prev_month = Month.from_num(calculation.monthnum) |> Month.prev
    indications_by_address_id(prev_month.num)
  end

  def t01_counters_sum(current_indications, previous_indications) do
    Enum.map(personal_addresses(), fn(address) ->
      current_indications[address.id].t01 - previous_indications[address.id].t01 - (current_indications[address.id].t01_corr || 0)
    end)
    |> Enum.sum
  end

  def t02_counters_sum(current_indications, previous_indications) do
    Enum.map(personal_addresses(), fn(address) ->
      current_indications[address.id].t02 - previous_indications[address.id].t02 - (current_indications[address.id].t02_corr || 0)
    end)
    |> Enum.sum
  end

  def personal_addresses do
    from(a in Address, where: not a.system and not a.archive)
    |> Repo.all
  end

  defp indications_by_address_id(monthnum) do
    from(i in AddressIndication, where: i.monthnum == ^monthnum)
    |> Repo.all
    |> group_by_address_id
  end

  defp group_by_address_id(objects) do
    Enum.reduce(objects, Map.new, fn(object, map) ->
      Map.put_new(map, object.address_id, object)
    end)
  end
end
