defmodule Bazhovka.AvgLoss do
  import Ecto.Query, only: [from: 2]

  alias Bazhovka.Repo
  alias Bazhovka.Month
  alias Bazhovka.Address
  alias Bazhovka.Calculation
  alias Bazhovka.AddressIndication
  alias Bazhovka.AddressCalculation

  @sys_counter_multiple_factor 20

  def calculate_counters_value(month, number_of_month_for_avg_calculation) do
    %{1 => [:t01, :t02], 2 => [:t01, :t02]}
    |> Enum.flat_map(fn({fider_num, tariffs}) ->
         Enum.reduce(tariffs, %{}, fn(tariff, acc) ->
           Map.put_new(
             acc,
             :'f#{fider_num}#{tariff}',
             calculate_counter_value(month, fider_num, tariff, number_of_month_for_avg_calculation)
           )
         end)
       end)
  end

  def calculate_counter_value(month, fider_num, tariff, number_of_month_for_avg_calculation)
      when tariff == :t01 or tariff == :t02 do

    this_month_cons_with_loss =
      counters_cons(month, fider_num, tariff) +
      avg_loss(Month.prev(month), fider_num, tariff, number_of_month_for_avg_calculation)

    sys_counter(Month.prev(month), fider_num, tariff) + this_month_cons_with_loss / @sys_counter_multiple_factor
  end

  def avg_loss(last_month, fider_num, tariff, number_of_month_for_avg_calculation)
      when tariff == :t01 or tariff == :t02 do

    month_losses =
      Stream.iterate(last_month, &Month.prev/1)
        |> Enum.take(number_of_month_for_avg_calculation)
        |> Enum.map(fn(monthnum) -> month_loss(monthnum, fider_num, tariff) end)

    Enum.sum(month_losses) / Enum.count(month_losses)
  end

  def month_loss(month, fider_num, tariff) do
    sys_cons(month, fider_num, tariff) - counters_cons(month, fider_num, tariff)
  end

  def sys_cons(month, fider_num, tariff) when tariff == :t01 or tariff == :t02 do
    diff =
      from(
        c in Calculation,
        select: %{
          "f1t01" => fragment("? - ?", c.f1t01curr, c.f1t01prev),
          "f1t02" => fragment("? - ?", c.f1t02curr, c.f1t02prev),
          "f2t01" => fragment("? - ?", c.f2t01curr, c.f2t01prev),
          "f2t02" => fragment("? - ?", c.f2t02curr, c.f2t02prev)
        },
        where: c.monthnum == ^month.num,
        order_by: [desc: c.id]
      )
      |> Repo.one
      |> Map.get("f#{fider_num}#{tariff}")

    diff * @sys_counter_multiple_factor
  end

  def sys_counter(month, fider_num, tariff) when tariff == :t01 or tariff == :t02 do
    from(
      i in AddressIndication,
      where: i.address_id == ^fider_num and i.monthnum == ^month.num
    )
    |> Repo.one
    |> Map.get(tariff)
  end

  def counters_cons(month, fider_num, tariff) when tariff == :t01 or tariff == :t02 do
    # AddressIndication плох тем, что там нарушается последовательность из-за смены счётчика у Шакировой
    from(
      a in Address,
      join: ac in AddressCalculation, on: ac.address_id == a.id,
      join: c in Calculation, on: c.id == ac.calculation_id,
      select: %{t01: sum(ac.t01_consumption), t02: sum(ac.t02_consumption)},
      where: c.monthnum == ^month.num
        and a.fider_num == ^fider_num
        and not a.system
        and fragment("? IN (SELECT id FROM (SELECT id, ROW_NUMBER() OVER (PARTITION BY monthnum) FROM calculations WHERE monthnum = ?) t WHERE row_number = 1)", c.id, ^month.num)
    )
    |> Repo.one
    |> Map.get(tariff)
  end
end
