defmodule Bazhovka.IndicationsReminderService do
  alias Bazhovka.Repo
  alias Bazhovka.Token
  alias Bazhovka.Mailer
  alias Bazhovka.Address

  import Ecto.Query, only: [from: 2]

  def send_to_all do
    expiration_dt = Timex.now("Asia/Yekaterinburg")
                    |> Timex.end_of_week
                    |> Timex.end_of_day
                    |> Timex.to_datetime("UTC")

    from(a in Address, where: not a.system and not a.archive, order_by: a.id, preload: [:users])
    |> Repo.all
    |> Enum.each(fn(address) ->
      Enum.each(address.users, fn(user) ->
        if user.email == address.email && confirm(address, user.email) do
          Token.create(to_string(user.id), expiration_dt)
          |> Mailer.indications(user.email, expiration_dt)
          |> Mailer.deliver
        end
      end)
    end)
  end

  defp confirm(address, email) do
    IO.gets("Отправить напоминание для #{address.name} - #{address.owner} на #{email} y/n?\n") == "y\n"
  end
end
