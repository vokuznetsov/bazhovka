defmodule Bazhovka.PaymentService do
  alias Bazhovka.Repo
  alias Bazhovka.Month
  alias Bazhovka.Address
  alias Bazhovka.Payment
  alias Bazhovka.PaymentReceipt

  import Ecto.Query, only: [from: 2]

  @payment_comments %{
    electro: %{payment: "Начислено за электроэнергию", receipt: "Оплата электроэнергии за "},
    member: %{payment: "Начислено по членским взносам", receipt: "Членский взнос за "},
    target: %{payment: "Начислено по целевым взносам", receipt: "Целевой взнос на "},
    lighting: %{payment: "Начислено за освещение", receipt: "Оплата за освещение "},
    init: %{payment: "Начислено по вступительным взносам", receipt: "Вступительный взнос"}
  }

  def create_payments(:electro, calculation) do
    Repo.transaction fn ->
      Enum.map(calculation.address_calculations, fn(ac) ->
        sum = Decimal.add(ac.t01_sum, ac.t02_sum) |> Decimal.minus
        month = Month.from_num(calculation.monthnum)

        payment = Payment.changeset(
          %Payment{},
          %{address_id: ac.address_id,
            sum: sum,
            monthnum: month.num,
            comment: comment(month, :electro, :payment),
            type: "electro"}
        ) |> Repo.insert!

        balance = Payment.balance(payment)
        receipt = PaymentReceipt.changeset(
          %PaymentReceipt{},
          %{payment_id: payment.id,
            address_id: ac.address_id,
            monthnum: calculation.monthnum,
            balance: balance,
            comment: comment(month, :electro, :receipt),
            type: payment.type,
            meta: %{t01_prev: ac.t01_prev,
                    t01_curr: ac.t01_curr,
                    t01_corr: ac.t01_corr,
                    t01_loss: ac.t01_loss,
                    t01_consumption: ac.t01_consumption,
                    t01_sum: ac.t01_sum,
                    t02_prev: ac.t02_prev,
                    t02_curr: ac.t02_curr,
                    t02_corr: ac.t02_corr,
                    t02_loss: ac.t02_loss,
                    t02_consumption: ac.t02_consumption,
                    t02_sum: ac.t02_sum,
                    loss_coeff: calculation.loss_coeff,
                    loss_const: calculation.loss_const,
                    t01_counters_sum: calculation.t01_counters_sum,
                    t02_counters_sum: calculation.t02_counters_sum,
                    t01_rate: calculation.t01_rate,
                    t02_rate: calculation.t02_rate,
                    t01_consumption_sum: calculation.t01_consumption,
                    t02_consumption_sum: calculation.t02_consumption,
                    t01_loss_sum: calculation.t01_loss,
                    t02_loss_sum: calculation.t02_loss,

                    gen_sum: Decimal.add(ac.t01_sum, ac.t02_sum),
                    total_sum: Decimal.add(ac.t01_sum, ac.t02_sum) |> Decimal.sub(balance)}}
        ) |> Repo.insert!

        {payment, receipt}
      end)
    end
  end

  def create_payments(type, month, sum) when type == :target and sum >= 0 do
    Repo.transaction fn ->
      adresses = from(a in Address, where: a.is_member and not a.system and not a.archive) |> Repo.all
      Enum.map(adresses, fn(address) ->
        create_payment(type, month, sum, address)
      end)
    end
  end

  def create_payments(type, month, sum) when type == :member and sum >= 0 do
    Repo.transaction fn ->
      adresses = from(a in Address, where: a.is_member and not a.is_free_member and not a.system and not a.archive) |> Repo.all
      Enum.map(adresses, fn(address) ->
        create_payment(type, month, sum, address)
      end)
    end
  end

  def create_payments(type, month, sum) when type == :lighting and sum >= 0 do
    Repo.transaction fn ->
      adresses = from(a in Address, where: not a.is_member and not a.system and not a.archive) |> Repo.all
      Enum.map(adresses, fn(address) ->
        create_payment(type, month, sum, address)
      end)
    end
  end

  def create_payment(type, month, sum, address) do
    payment = Payment.changeset(
      %Payment{},
      %{address_id: address.id,
        sum: Decimal.new(sum) |> Decimal.minus,
        monthnum: month.num,
        comment: comment(month, type, :payment),
        type: to_string(type)}
    ) |> Repo.insert!

    balance = Payment.balance(payment)
    receipt = PaymentReceipt.changeset(
      %PaymentReceipt{},
      %{payment_id: payment.id,
        address_id: payment.address_id,
        monthnum: payment.monthnum,
        balance: balance,
        comment: comment(month, type, :receipt),
        type: payment.type,
        meta: %{gen_sum: payment.sum |> Decimal.minus,
                total_sum: Decimal.add(payment.sum, balance) |> Decimal.minus}}
    ) |> Repo.insert!

    {payment, receipt}
  end

  def last_payment_month(address_id, default) do
    monthnum = from(pr in PaymentReceipt, select: max(pr.monthnum), where: pr.address_id == ^address_id)
    |> Repo.one

    if (monthnum) do
      Month.from_num(monthnum)
    else
      default
    end
  end

  defp comment(_month, type, :payment) do
    Map.get(@payment_comments, type).payment
  end

  defp comment(month, type, :receipt) do
    base_text = Map.get(@payment_comments, type).receipt
    case type do
      :target -> base_text <> to_string(month.year) <> "г."
      :init -> base_text
      _ -> base_text <> month.name
    end
  end
end
