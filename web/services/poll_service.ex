defmodule Bazhovka.PollService do
  alias Bazhovka.Poll
  alias Bazhovka.Repo

  import Ecto.Query, only: [from: 2]

  def load_polls do
    from(
      p in Poll,
      preload: [:votes],
      order_by: [desc: p.id]
    ) |> Repo.all
  end

  def load_polls(poll_id) when is_integer(poll_id) do
    load_polls([poll_id]) |> List.first
  end

  def load_polls(poll_ids) when is_list(poll_ids) do
    from(
      p in Poll,
      preload: [:votes],
      where: p.id in ^poll_ids,
      order_by: [desc: p.id]
    ) |> Repo.all
  end
end
