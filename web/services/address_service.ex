defmodule Bazhovka.AddressService do
  alias Bazhovka.Repo

  def user_address(user) do
    addresses = Repo.preload(user, :addresses).addresses
    Enum.find(addresses, fn(a) -> a.email == user.email end)
  end
end