defmodule Bazhovka.Mailer do
  alias Bazhovka.Month
  alias Bazhovka.Repo
  alias Bazhovka.PaymentReceipt

  def payment_receipt(%PaymentReceipt{type: "electro"} = payment_receipt) do
    payment_receipt = Repo.preload(payment_receipt, [:address, :payment])
    month = Month.from_num(payment_receipt.monthnum)

    html = Phoenix.View.render_to_string(Bazhovka.PaymentReceiptView, "electro.html", payment_receipt: payment_receipt)
    pdf_filename = PdfGenerator.generate!(html)

    %Mailman.Email{
      subject: "Квитанция. #{payment_receipt.comment}",
      from: "sopk-bazhovka1@yandex.ru",
      to: [payment_receipt.address.email],
      html: Phoenix.View.render_to_string(Bazhovka.MailView, "electro.html", payment_receipt: payment_receipt, month: month),
      attachments: [
        Mailman.Attachment.inline!(pdf_filename, "e_payment_#{month.num}.pdf")
      ]
    }
  end

  def payment_receipt(%PaymentReceipt{type: type} = payment_receipt) do
    payment_receipt = Repo.preload(payment_receipt, [:address, :payment])
    month = Bazhovka.Month.from_num(payment_receipt.monthnum)

    html = Phoenix.View.render_to_string(Bazhovka.PaymentReceiptView, "#{type}.html", payment_receipt: payment_receipt)
    pdf_filename = PdfGenerator.generate!(html)

    %Mailman.Email{
      subject: "Квитанция. #{payment_receipt.comment}",
      from: "sopk-bazhovka1@yandex.ru",
      to: [payment_receipt.address.email],
      html: "",
      attachments: [
        Mailman.Attachment.inline!(pdf_filename, "m_payment_#{month.num}.pdf")
      ]
    }
  end

  def reset_password(token_id, email) do
    %Mailman.Email{
      subject: "Сброс пароля сайта",
      from: "sopk-bazhovka1@yandex.ru",
      to: [email],
      html: Phoenix.View.render_to_string(Bazhovka.MailView, "reset_password.html", token_id: token_id)
    }
  end

  def welcome(token_id, email) do
    %Mailman.Email{
      subject: "Добро пожаловать на сайт Бажовки",
      from: "sopk-bazhovka1@yandex.ru",
      to: [email],
      html: Phoenix.View.render_to_string(Bazhovka.MailView, "welcome.html", token_id: token_id)
    }
  end

  def news(token_id, post, email) do
    %Mailman.Email{
      subject: "На нашем сайте есть новости",
      from: "sopk-bazhovka1@yandex.ru",
      to: [email],
      html: Phoenix.View.render_to_string(Bazhovka.MailView, "news.html", post: post, token_id: token_id)
    }
  end

  def indications(token_id, email, due_date) do
    %Mailman.Email{
      subject: "Напоминание о необходимости передачи показаний счётчиков",
      from: "sopk-bazhovka1@yandex.ru",
      to: [email],
      html: Phoenix.View.render_to_string(
        Bazhovka.MailView, "indications.html",
        token_id: token_id,
        due_date: due_date
      )
    }
  end

  def deliver(email) do
    Mailman.deliver(email, config())
  end

  def config do
    %Mailman.Context{
      config: nil,
      composer: %Mailman.EexComposeConfig{}
    }
  end
end
