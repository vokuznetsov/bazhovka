defmodule Bazhovka.Repo.Migrations.CreateAddress do
  use Ecto.Migration

  def change do
    create table(:addresses) do
      add :name, :string, null: false
      add :account_num, :string
      add :fider_num, :integer
      add :system, :boolean, null: false
      add :owner, :string

      timestamps()
    end

  end
end
