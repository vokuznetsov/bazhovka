defmodule Bazhovka.Repo.Migrations.DowncaseUserEmails do
  use Ecto.Migration

  def up do
    execute "UPDATE users SET email = lower(email)"
  end

  def down do
    # nothing
  end
end
