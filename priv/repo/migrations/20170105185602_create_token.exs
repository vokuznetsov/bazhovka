defmodule Bazhovka.Repo.Migrations.CreateToken do
  use Ecto.Migration

  def change do
    create table(:tokens, primary_key: false) do
      add :uid, :string, primary_key: true
      add :value, :string
      add :expiration_time, :utc_datetime

      timestamps()
    end

  end
end
