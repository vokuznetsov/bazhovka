defmodule Bazhovka.Repo.Migrations.AddSendFlagsToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :send_electro_receipts, :boolean, default: false, null: false
      add :send_member_receipts, :boolean, default: false, null: false
    end
  end
end
