defmodule Bazhovka.Repo.Migrations.CreateFolder do
  use Ecto.Migration

  def change do
    create table(:folders) do
      add :name, :string
      add :parent_id, references(:folders, on_delete: :nothing)

      timestamps()
    end
    create index(:folders, [:parent_id])
  end
end
