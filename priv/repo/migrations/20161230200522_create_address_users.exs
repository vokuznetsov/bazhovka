defmodule Bazhovka.Repo.Migrations.CreateAddressUsers do
  use Ecto.Migration

  def change do
    create table(:address_users, primary_key: false) do
      add :address_id, references(:addresses, on_delete: :delete_all)
      add :user_id, references(:users, on_delete: :delete_all)
    end

    create index(:address_users, :address_id)
    create index(:address_users, :user_id)
  end
end
