defmodule Bazhovka.Repo.Migrations.CreateAddressIndication do
  use Ecto.Migration

  def change do
    create table(:address_indications) do
      add :t01, :integer, null: false
      add :t02, :integer, null: false
      add :monthnum, :integer, null: false
      add :address_id, references(:addresses, on_delete: :nothing), null: false

      timestamps()
    end
    create unique_index(:address_indications, [:address_id, :monthnum])

  end
end
