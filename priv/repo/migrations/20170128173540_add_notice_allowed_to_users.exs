defmodule Bazhovka.Repo.Migrations.AddNoticeAllowedToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :notice_allowed, :boolean, default: false
    end
  end
end
