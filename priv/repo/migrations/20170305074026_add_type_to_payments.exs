defmodule Bazhovka.Repo.Migrations.AddTypeToPayments do
  use Ecto.Migration

  def up do
    alter table(:payments) do
      add :type, :string
    end

    execute "UPDATE payments SET type = 'electro'"

    alter table(:payments) do
      modify :type, :string, null: false
    end
  end

  def down do
    alter table(:payments) do
      remove :type
    end
  end
end
