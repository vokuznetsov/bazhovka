defmodule Bazhovka.Repo.Migrations.AddMonthnumToPayments do
  use Ecto.Migration

  def change do
    alter table(:payments) do
      add :monthnum, :integer, null: false
    end

    create index(:payments, [:monthnum])
  end
end
