defmodule Bazhovka.Repo.Migrations.AddStateToPaymentReceipts do
  use Ecto.Migration

  def change do
    alter table(:payment_receipts) do
      add :state, :string, default: "not_sent", null: false
    end
  end
end
