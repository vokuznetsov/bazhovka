defmodule Bazhovka.Repo.Migrations.CreateVote do
  use Ecto.Migration

  def change do
    create table(:votes) do
      add :answer, :string
      add :address_id, references(:addresses, on_delete: :nothing)
      add :poll_id, references(:polls, on_delete: :nothing)

      timestamps()
    end

    create unique_index(:votes, [:poll_id, :address_id])
  end
end
