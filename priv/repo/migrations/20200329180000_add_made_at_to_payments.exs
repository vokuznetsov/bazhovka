defmodule Bazhovka.Repo.Migrations.AddMadeAtToPayments do
  use Ecto.Migration

  def change do
    alter table(:payments) do
      add :made_at, :date
    end
  end
end
