defmodule Bazhovka.Repo.Migrations.CreatePayment do
  use Ecto.Migration

  def change do
    create table(:payments) do
      add :sum, :decimal, precision: 10, scale: 2
      add :comment, :string
      add :address_id, references(:addresses, on_delete: :nothing)

      timestamps()
    end
    create index(:payments, [:address_id])

  end
end
