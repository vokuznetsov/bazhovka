defmodule Bazhovka.Repo.Migrations.CreateCalculation do
  use Ecto.Migration

  def change do
    create table(:calculations) do
      add :monthnum, :integer
      add :f1t01curr, :integer
      add :f1t02curr, :integer
      add :f1t01prev, :integer
      add :f1t02prev, :integer
      add :f2t01curr, :integer
      add :f2t02curr, :integer
      add :f2t01prev, :integer
      add :f2t02prev, :integer
      add :lighting_t01_curr, :integer
      add :lighting_t02_curr, :integer
      add :lighting_t01_prev, :integer
      add :lighting_t02_prev, :integer
      add :entrance_t01_curr, :integer
      add :entrance_t02_curr, :integer
      add :entrance_t01_prev, :integer
      add :entrance_t02_prev, :integer
      add :multiple_factor, :integer
      add :loss_coeff, :decimal, precision: 10, scale: 5
      add :loss_const, :integer
      add :t01_rate, :decimal, precision: 10, scale: 2
      add :t02_rate, :decimal, precision: 10, scale: 2
      add :lighting_proportion, :decimal, precision: 6,  scale: 6
      add :entrance_proportion, :decimal, precision: 6,  scale: 6
      add :t01_consumption, :integer
      add :t02_consumption, :integer
      add :t01_loss_proportional, :integer
      add :t02_loss_proportional, :integer
      add :gen_t01_sum, :decimal, precision: 10, scale: 2
      add :gen_t02_sum, :decimal, precision: 10, scale: 2
      add :lighting_t01_consumption, :integer
      add :lighting_t02_consumption, :integer
      add :entrance_t01_consumption, :integer
      add :entrance_t02_consumption, :integer
      add :lighting_t01_sum, :decimal, precision: 10, scale: 2
      add :lighting_t02_sum, :decimal, precision: 10, scale: 2
      add :entrance_t01_sum, :decimal, precision: 10, scale: 2
      add :entrance_t02_sum, :decimal, precision: 10, scale: 2
      add :t01_loss, :integer
      add :t02_loss, :integer
      add :t01_counters_sum, :integer
      add :t02_counters_sum, :integer
      add :calculation_class, :string

      timestamps()
    end

  end
end
