defmodule Bazhovka.Repo.Migrations.AddFoldersToDocuments do
  use Ecto.Migration

  def up do
    alter table(:documents) do
      add :folder_id, references(:folders, on_delete: :nothing)
    end

    create index(:documents, [:folder_id])

    execute """
      WITH f as (
        INSERT INTO folders (name, inserted_at, updated_at) VALUES ('Все документы', now(), now()) RETURNING id
      )

      UPDATE documents SET folder_id = f.id FROM f;
    """

    alter table(:documents) do
      modify :folder_id, :integer, null: false
    end
  end

  def down do
    alter table(:documents) do
      remove :folder_id
    end

    execute """
      DELETE FROM folders WHERE name = 'Все документы';
    """
  end
end
