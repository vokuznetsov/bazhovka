defmodule Bazhovka.Repo.Migrations.AddAvgCalculatedToIndications do
  use Ecto.Migration

  def change do
    alter table(:address_indications) do
      add :avg_calculated, :boolean
    end
  end
end
