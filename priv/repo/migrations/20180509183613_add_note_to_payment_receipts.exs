defmodule Bazhovka.Repo.Migrations.AddNoteToPaymentReceipts do
  use Ecto.Migration

  def change do
    alter table(:payment_receipts) do
      add :note, :text
    end
  end
end
