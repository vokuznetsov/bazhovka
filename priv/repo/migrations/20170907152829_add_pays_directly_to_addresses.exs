defmodule Bazhovka.Repo.Migrations.AddPaysDirectlyToAddresses do
  use Ecto.Migration

  def change do
    alter table(:addresses) do
      add :pays_directly, :boolean
    end
  end
end
