defmodule Bazhovka.Repo.Migrations.CreatePost do
  use Ecto.Migration

  def change do
    create table(:posts) do
      add :caption, :string
      add :body, :text

      timestamps()
    end

  end
end
