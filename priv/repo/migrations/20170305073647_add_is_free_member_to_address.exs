defmodule Bazhovka.Repo.Migrations.AddIsFreeMemberToAddress do
  use Ecto.Migration

  def change do
    alter table(:addresses) do
      add :is_free_member, :boolean, null: false, default: false
    end
  end
end
