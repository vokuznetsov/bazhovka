defmodule Bazhovka.Repo.Migrations.CreatePaymentReceipt do
  use Ecto.Migration

  def change do
    create table(:payment_receipts) do
      add :monthnum, :integer
      add :address_id, references(:addresses, on_delete: :nothing)
      add :payment_id, references(:payments, on_delete: :nothing)
      add :meta, :map
      add :balance, :decimal, precision: 10, scale: 2

      timestamps()
    end
    create index(:payment_receipts, [:address_id])
    create index(:payment_receipts, [:monthnum])
  end
end
