defmodule Bazhovka.Repo.Migrations.AddIsMemberToAddress do
  use Ecto.Migration

  def change do
    alter table(:addresses) do
      add :is_member, :boolean
    end
  end
end
