defmodule Bazhovka.Repo.Migrations.AddCorrectionsToAddressCalculation do
  use Ecto.Migration

  def change do
    alter table(:address_calculations) do
      add :t01_corr, :integer
      add :t02_corr, :integer
    end
  end
end
