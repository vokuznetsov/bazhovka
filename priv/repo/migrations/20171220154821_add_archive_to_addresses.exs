defmodule Bazhovka.Repo.Migrations.AddArchiveToAddresses do
  use Ecto.Migration

  def change do
    alter table(:addresses) do
      add :archive, :boolean, default: false
    end
  end
end
