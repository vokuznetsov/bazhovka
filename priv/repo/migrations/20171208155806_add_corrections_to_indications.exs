defmodule Bazhovka.Repo.Migrations.AddCorrectionsToIndications do
  use Ecto.Migration

  def change do
    alter table(:address_indications) do
      add :t01_corr, :integer
      add :t02_corr, :integer
    end
  end
end
