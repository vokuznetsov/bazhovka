defmodule Bazhovka.Repo.Migrations.CreateAddressCorrection do
  use Ecto.Migration

  def change do
    create table(:address_corrections) do
      add :t01, :decimal, precision: 10, scale: 2
      add :t02, :decimal, precision: 10, scale: 2
      add :monthnum, :integer
      add :address_id, references(:addresses, on_delete: :nothing)

      timestamps()
    end
    create index(:address_corrections, [:address_id])

  end
end
