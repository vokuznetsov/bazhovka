defmodule Bazhovka.Repo.Migrations.AddTypeToPaymentReceipts do
  use Ecto.Migration

  def change do
    alter table(:payment_receipts) do
      add :type, :string, null: false
    end
  end
end
