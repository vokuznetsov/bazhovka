defmodule Bazhovka.Repo.Migrations.AddCommercialLossToCalculations do
  use Ecto.Migration

  def change do
    alter table(:calculations) do
      add :t01_loss_commercial, :integer, default: 0
      add :t02_loss_commercial, :integer, default: 0
    end
  end
end
