defmodule Bazhovka.Repo.Migrations.AlterLossCoeffPrecision do
  use Ecto.Migration

  def up do
    alter table(:calculations) do
      modify :loss_coeff, :decimal, precision: 10, scale: 3
    end
  end

  def down do
    alter table(:calculations) do
      modify :loss_coeff, :decimal, precision: 10, scale: 5
    end
  end
end
