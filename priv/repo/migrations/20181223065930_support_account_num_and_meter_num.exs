defmodule Bazhovka.Repo.Migrations.SupportAccountNumAndMeterNum do
  use Ecto.Migration

  def up do
    alter table(:addresses) do
      add :meter_num, :string
    end

    execute "UPDATE addresses SET meter_num = account_num, account_num = null"
  end

  def down do
    execute "UPDATE addresses SET account_num = meter_num"

    alter table(:addresses) do
      remove :meter_num
    end
  end
end
