defmodule Bazhovka.Repo.Migrations.CreateAddressCalculation do
  use Ecto.Migration

  def change do
    create table(:address_calculations) do
      add :t01_curr, :integer
      add :t01_prev, :integer
      add :t02_curr, :integer
      add :t02_prev, :integer
      add :t01_loss, :decimal, precision: 10, scale: 4
      add :t02_loss, :decimal, precision: 10, scale: 4
      add :t01_sum, :decimal, precision: 10, scale: 2
      add :t02_sum, :decimal, precision: 10, scale: 2
      add :t01_consumption, :integer
      add :t02_consumption, :integer
      add :t01_correction, :decimal, precision: 10, scale: 2
      add :t02_correction, :decimal, precision: 10, scale: 2
      add :address_id, references(:addresses, on_delete: :nothing)
      add :calculation_id, references(:calculations, on_delete: :nothing)
    end
    create index(:address_calculations, [:address_id])
    create index(:address_calculations, [:calculation_id])

  end
end
