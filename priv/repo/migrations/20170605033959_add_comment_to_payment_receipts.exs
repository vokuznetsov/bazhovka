defmodule Bazhovka.Repo.Migrations.AddCommentToPaymentReceipts do
  use Ecto.Migration

  def up do
    alter table(:payment_receipts) do
      add :comment, :string
    end

    execute """
      UPDATE payment_receipts SET comment = meta->>'text' WHERE meta ? 'text'
    """
  end

  def down do
    alter table(:payment_receipts) do
      remove :comment
    end
  end
end
