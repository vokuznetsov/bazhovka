defmodule Bazhovka.Repo.Migrations.CreateDocument do
  use Ecto.Migration

  def change do
    create table(:documents) do
      add :name, :string, null: false
      add :path, :string, null: false

      timestamps()
    end

  end
end
