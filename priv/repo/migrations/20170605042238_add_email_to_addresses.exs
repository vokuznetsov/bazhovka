defmodule Bazhovka.Repo.Migrations.AddEmailToAddresses do
  use Ecto.Migration

  def change do
    alter table(:addresses) do
      add :email, :string
    end
  end
end
